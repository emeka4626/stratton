<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'username', 'password', 'referral_code', 'passcode'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getRouteKeyName()
    {
        return 'username';
    }

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function name()
    {
        return ucwords($this->name);
    }

    public function firstName()
    {
        return ucwords(collect(explode(" ", $this->name))->first());
    }

    public function transactions()
    {
        return $this->belongsToMany(Transaction::class, 'user_transaction', 'user_id', 'transaction_id');
    }

    public function btcBalance($status)
    {
        return collect($this->transactions()->where('status', $status)->where('type', '!=', 'wallet')->get())
            ->map(fn($transaction) => $transaction->value)
            ->sum();
    }

    public function usdBalance($status)
    {
        return collect($this->transactions()->where('status', $status)
            ->where('type','wallet')->get())
            ->map(fn($transaction) => $transaction->amount)
            ->sum();
    }

    public function totalUsdBalance()
    {
        return '$' . number_format($this->usdBalance('complete') + $this->usdInvestmentBalance());
    }

    public function verification()
    {
        return $this->hasOne(Verification::class);
    }

    public function isVerified()
    {
        if ($this->verification) {
            return $this->verification->isVerified();
        }
         return false;
    }

    public function banks()
    {
        return $this->hasMany(Bank::class);
    }

    public function cards()
    {
        return $this->hasMany(Card::class);
    }

    public function investments()
    {
        return $this->hasMany(Investment::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_user');
    }

    public function hasRole(String $role) : bool
    {
        return (bool) $this->roles()->where('name', $role)->first();
    }

    public function account()
    {
        return $this->hasOne(Account::class);
    }

    /** add to account balance
     * @param $amount
     */
    public function addBalance($amount): void
    {
        if ($this->account){
            $this->account->balance += $amount;
            $this->account->save();
        }
    }

    private function usdInvestmentBalance()
    {
        return collect($this->investments()->where('status', '!=', 'PENDING')->get())
            ->map(fn(Investment $investment) => $investment->earning())
            ->sum();
    }

    public function withdrawals()
    {
        return $this->hasMany(Withdrawal::class);
    }

    public function accountBalance()
    {
        return $this->account->balance();

    }

    public function referrals()
    {
        return $this->hasMany(Referral::class);
    }
}
