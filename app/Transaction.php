<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $guarded = [];

    protected $with = ['crypto'];

    public function user()
    {
        return $this->belongsToMany(User::class, 'user_transaction');
    }

    public function crypto()
    {
        return $this->belongsToMany(Crypto::class, 'crypto_transaction');
    }

    public function getUser()
    {
        return $this->user()->first();
    }
}
