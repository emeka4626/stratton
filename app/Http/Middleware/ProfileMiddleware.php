<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ProfileMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->profile) {
            return $next($request);
        }

        session()->flash('warning', 'you need to create a profile to proceed');
        return redirect()->route('profile.create');
    }
}
