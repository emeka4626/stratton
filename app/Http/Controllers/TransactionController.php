<?php

namespace App\Http\Controllers;

use App\Modules\BlockChainModule;
use App\Notifications\CompletePurchase;
use App\Repositories\TransactionRepository;
use App\Transaction;
use Blockchain\Exception\Error;
use Blockchain\Exception\HttpError;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class TransactionController extends Controller
{

    public function __construct(TransactionRepository $transactionRepository, BlockChainModule $blockChainModule)
    {
        $this->transactionRepository = $transactionRepository;
        $this->blockChainModule = $blockChainModule;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Transaction|string
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'amount' => ['required'],
            'volume' => ['required']
        ]);

        $code = uniqid(Str::random(10));
        $transaction = $this->transactionRepository->createTransaction($request, $code);
        // attach user id to the transaction
        $transaction->user()->attach(auth()->id());

        /*
        * attach the cryptocurrency id to the transaction
        * default 1 is bitcoin
        * useful for adding multi currency support
        */
        $transaction->crypto()->attach(1);

        auth()->user()->notify(new CompletePurchase(auth()->user()));

        return $transaction->load(['user', 'crypto']);


    }

    /**
     * Display the specified resource.
     *
     * @param \App\Transaction $transaction
     * @return Response
     */
    public function show(Transaction $transaction)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Transaction $transaction
     * @return Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }
}
