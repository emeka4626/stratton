<?php

namespace App\Http\Controllers\Admin;

use App\Account;
use App\Http\Controllers\Controller;
use App\Investment;
use App\Plan;
use App\Repositories\TransactionRepository;
use App\Testimonial;
use App\Transaction;
use App\User;
use App\Withdrawal;
use Blockchain\Blockchain;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;
use Intervention\Image\Facades\Image;

class BaseController extends Controller
{
    /**
     * @var TransactionRepository
     */
    private TransactionRepository $repository;
    /**
     * @var Blockchain
     */
    private Blockchain $blockchain;

    public function __construct(TransactionRepository $repository, Blockchain $blockchain)
    {
        $this->repository = $repository;
        $this->blockchain = $blockchain;
    }

    /**
     * @param User $username
     * @return Application|Factory|View
     */
    public function index(User $username)
    {
        $investments = Investment::sum('amount');
        $transactions = Transaction::sum('amount');
        $withdrawals = Withdrawal::sum('amount');
        $users = User::count();

        $balance = $this->getCompleteTransactionTotal() + $this->getCompleteInvestmentTotal();
        return view('admin.index', compact('username', 'investments', 'transactions', 'withdrawals', 'users', 'balance'));
    }

    public function btcTransactions(User $username)
    {
        $page = "Bitcoin Transactions";
        $transactions = Transaction::with('user')->latest('created_at')->get();
        return view('admin.btc-transactions', compact(['username', 'transactions', 'page']));

    }
    public function pendingTransactions(User $username)
    {
        $page = "Pending Transactions";
        $transactions = Transaction::with('user')->where('status', 'pending')->latest('created_at')->get();
        return view('admin.btc-transactions', compact(['username', 'transactions', 'page']));

    }

    public function completedTransactions(User $username)
    {
        $page = "completed Transactions";
        $transactions = Transaction::with('user')->where('status', 'complete')->latest('created_at')->get();
        return view('admin.btc-transactions', compact(['username', 'transactions', 'page']));

    }

    public function fundWallet(User $username)
    {
        return \view('admin.fund-wallet');
    }

    /**
     * @param Request $request
     * @param User $username
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function saveFund(Request $request, User $username)
    {
        $this->validate($request, [
            'username' => ['required', 'string'],
            'amount' => ['required']
        ]);


        if ($user = User::whereUsername($request->input('username'))->first()) {
            $btcValue = $this->blockchain->Rates->toBTC($request->input('amount'), 'USD');

            $code = uniqid(Str::random(10));

            $transaction = Transaction::create([
                'code' => $code,
                'amount' => $request->input('amount'),
                'value' => $btcValue,
                'status' => 'complete',
                'type' => 'wallet'
            ]);

            $transaction->user()->attach($user);

            $transaction->crypto()->attach(1);

            $account = Account::where('user_id', $transaction->user()->first()->id)->first();
            $account->balance += $transaction->amount;
            $account->save();


            session()->flash('success', 'wallet funded successfully');

            return redirect()->back();
        }

        session()->flash('error', 'User not Found');

        return redirect()->back();
    }

    public function investments(User $username)
    {
        $page = "Investments";
        $investments = Investment::with('user')->latest('starts')->get();
        return view('admin.investments', compact(['page', 'investments']));
    }

    public function confirmTransaction(User $username, Transaction $transaction)
    {
        $transaction->load('user');
        $transaction->status = 'complete';
        $transaction->save();


        if ($transaction->type === 'wallet') {

            $account = Account::where('user_id', $transaction->user()->first()->id)->first();

            $account->balance += $transaction->amount;
            $account->save();
        }

        session()->flash('success', 'Transaction Confirmed');

        return redirect()->back();
    }


    public function confirmInvestment(User $username, Investment $investment)
    {
        $investment->load('user');
        $investment->status = 'ACTIVE';
        $investment->save();

        $account = Account::where('user_id', $investment->user->id)->first();

        $account->balance += $investment->earning();

        $account->save();

        session()->flash('success', 'Investment Started');

        return redirect()->back();
    }

    public function pendingInvestments(User $username)
    {
        $page = "Pending Investments";
        $investments = Investment::with('user')->where('status', 'PENDING')->latest('starts')->get();
        return view('admin.investments', compact(['username', 'investments', 'page']));

    }

    public function confirmedInvestments(User $username)
    {
        $page = "Confirmed Investments";
        $investments = Investment::with('user')->where('status', '!=', 'PENDING')->latest('starts')->get();
        return view('admin.investments', compact(['username', 'investments', 'page']));

    }

    public function addFund()
    {
        return view('admin.add-fund');
    }

    /**
     * @param Request $request
     * @param User $username
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function fund(Request $request, User $username)
    {
        $this->validate($request, [
            'username' => ['required', 'string'],
            'plan' => 'required',
            'amount' => ['required']
        ]);

        if ($user = User::whereUsername($request->input('username'))->first()) {


            $plan = Plan::findorFail($request->input('plan'));
            $investment = $user->investments()->create([
                'plan_id' => $plan->id,
                'amount' => $request->input('amount'),
                'status' => 'ACTIVE',
                'starts' => Carbon::now(),
                'ends' => Carbon::now()->addDays($plan->duration)
            ]);

            $account = Account::where('user_id', $investment->user->id)->first();

            $account->balance += $investment->earning();

            $account->save();

            session()->flash('success', 'Account funded successfully');

            return redirect()->back();
        }

        session()->flash('error', 'User not Found');

        return redirect()->back();
    }

    public function users(User $username)
    {
        $users = User::all();
        return view('admin.users', compact('users'));
    }

    public function editUser(User $username, User $user)
    {

        return view('admin.edit-user',compact('user'));
    }

    public function UpdateUser(Request $request, User $username, User $user)
    {
        $this->validate($request, [
            'name' => ['required'],
            'username' => ['required', 'unique:users'],
            'email' => ['required', 'unique:users'],
        ]);

        $user->name = $request->input('name');
        $user->username = $request->input('username');
        $user->email = $request->input('email');
        if (isset($request['password'])) {
            $user->passcode = $request->input('password');
            $user->password = bcrypt($request->input('password'));
        }
        $user->save();

        session()->flash('success', 'Account updated successfully');

        return redirect()->route('admin.users');
    }

    public function settings(User $username)
    {
        $plans = Plan::count();
        $testimonials = Testimonial::count();
        return view('admin.settings', compact(['plans', 'testimonials']));
    }

    public function settingsPlans(User $username)
    {
        $plans = Plan::all();
        return view('admin.settings-plans', compact('plans'));
    }

    public function settingsTestimonials(User $username)
    {
        $plans = Plan::all();
        return view('admin.settings-testimonials', compact('plans'));
    }

    public function settingsCreatePlan(User $username)
    {
        return view('admin.create-plan');
    }

    public function createPlan(Request $request, User $username)
    {
        $this->validate($request, [
            'name' => ['required'],
            'duration' => ['required'],
            'percentage' => ['required'],
            'min' => ['required'],
            'max' => ['required'],
        ]);

        Plan::create($request->all());

        session()->flash('success', 'plan created');

        return redirect()->route('admin.settings.plans', $username);
    }

    public function createTestimonial(Request $request, User $username)
    {

        $this->validate($request, [
            'name' => ['required'],
            'text' => ['required'],
            'image' => ['required'],
            'company' => ['required'],
        ]);

        $testimonial = Testimonial::create([
            'name' => $request->input('name'),
            'text' => $request->input('text'),
            'company' => $request->input('company')
        ]);

        $this->uploadTestimonialImage($request, $testimonial);

        session()->flash('success', 'testimonial added');

        return redirect()->route('admin.settings', $username);
    }

    private function uploadTestimonialImage(Request $request, Testimonial $testimonial)
    {
        $image = $request->file('image');

        $tempName = rand(0, 5).time().'.'.$image->getClientOriginalExtension();

        $savePath = public_path('uploads/testimonials/'.$tempName);

        Image::make($image->getRealPath())->save($savePath);

        $testimonial->image = $tempName;

        $testimonial->save();
    }

    private function getCompleteInvestmentTotal()
    {
        return Investment::where('status', '!=', 'PENDING')->sum('amount');
    }

    private function getCompleteTransactionTotal()
    {
        return Transaction::where('status', '!=', 'pending')->sum('amount');
    }

    public function withdrawals(User $username)
    {
        $withdrawals = Withdrawal::with('user')->latest('created_at')->get();

        return \view('admin.withdrawals', compact('withdrawals'));
    }

}
