<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Intervention\Image\Constraint;
use Intervention\Image\Facades\Image;
use Intervention\Image\Size;

class ProfileController extends Controller
{

    public function index()
    {
        $user = auth()->user();
        return view('profiles.create', compact('user'));
    }

    /** create a profile for a user
     * @param Request $request
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'address' => ['required', 'min:8'],
            'phone' => ['required', 'min:6'],
            'city' => ['required'],
            'country' => ['required'],
            'birthday' => ['required'],
            'postal_code' => ['required', 'min:4']
        ]);

        // create user profile
        $profile = Profile::create([
            'user_id' => auth()->id(),
           'phone' => $request->input('phone'),
           'address' => $request->input('address'),
           'city' => $request->input('city'),
           'country' => $request->input('country'),
           'postal_code' => $request->input('postal_code'),
           'birthday' => $request->input('birthday'),
        ]);

//        $this->uploadProfileAvatar($profile, $request);

        return redirect()->route('account.dashboard', ['username' => auth()->user()->username ]);
    }

    /** attach an avatar to a user profile
     * @param Profile $profile
     * @param Request $request
     */
    private function uploadProfileAvatar(Profile $profile, Request $request)
    {
        $image = $request->file('avatar');

        $tempName = rand(0, 5).time().'.'.$image->getClientOriginalExtension();

        $savePath = public_path('uploads/users/'.$tempName);

        Image::make($image->getRealPath())->save($savePath);

        $profile->avatar = $tempName;

        $profile->save();
    }
}
