<?php

namespace App\Http\Controllers;

use App\Bank;
use App\Card;
use App\Verification;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;
use Intervention\Image\Facades\Image;

class VerificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('account.verification.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        return  view('account.verification.verify-id');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'front' => ['required'],
            'back' => ['required']
        ]);

        $verification = Verification::create([
            'user_id' => auth()->id()
        ]);

        $this->uploadFrontImage($verification, $request);
        $this->uploadBackImage($verification, $request);

        session()->flash('success','your ID verification is pending');

        return redirect()->route('account.settings-account', auth()->user());
    }

    /**
     * Display the specified resource.
     *
     * @return Application|Factory|View
     */
    public function addCard()
    {
        return view('account.verification.add-card');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return Application|Factory|View
     */
    public function addBank()
    {
        return \view('account.verification.add-bank');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function saveBank(Request $request)
    {
        $this->validate($request, [
            'name' => ['required'],
            'routing' => ['required'],
            'number' => ['required'],
        ]);

        Bank::create([
            'user_id' => auth()->id(),
            'name' => $request->input('name'),
            'routing' => $request->input('routing'),
            'number' => $request->input('number'),
        ]);
        session()->flash('success', 'bank details added successfully');

        return redirect()->route('account.settings-account', auth()->user());
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function saveCard(Request $request)
    {

        $this->validate($request, [
            'number' => ['required'],
            'expiration' => ['required'],
            'cvc' => ['required'],
            'postal' => ['required'],
        ]);

        Card::create([
            'user_id' => auth()->id(),
            'number' => $request->input('number'),
            'expiration' => $request->input('expiration'),
            'cvc' => $request->input('cvc'),
            'postal' => $request->input('postal'),
        ]);
        session()->flash('success', 'card details added successfully');

        return redirect()->route('account.settings-account', auth()->user());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Verification  $verification
     * @return Response
     */
    public function destroy(Verification $verification)
    {
        //
    }

    private function uploadFrontImage(Verification $verification, Request $request)
    {
        $image = $request->file('front');

        $tempName = rand(0, 5).time().'.'.$image->getClientOriginalExtension();

        $savePath = public_path('uploads/verification/'.$tempName);

        Image::make($image->getRealPath())->save($savePath);

        $verification->front = $tempName;

        $verification->save();
    }

    private function uploadBackImage(Verification $verification, Request $request)
    {
        $image = $request->file('back');

        $tempName = rand(0, 5).time().'.'.$image->getClientOriginalExtension();

        $savePath = public_path('uploads/verification/'.$tempName);

        Image::make($image->getRealPath())->save($savePath);

        $verification->back = $tempName;

        $verification->save();
    }
}
