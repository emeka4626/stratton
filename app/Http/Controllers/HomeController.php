<?php

namespace App\Http\Controllers;

use App\Plan;
use App\Repositories\UserRepository;
use App\Transaction;
use App\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->middleware('auth');
        $this->userRepository = $userRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @param User $username
     * @return Renderable
     */
    public function index(User $username)
    {

        $transactions = $username->transactions;
        $plans = Plan::all();
        $investments = $username->investments;
        return view('home', compact('transactions', 'plans', 'investments'));
    }


    /**
     * @param User $username
     * @return Application|Factory|View
     */
    public function buy(User $username)
    {
        $transactions = $username->transactions;

        return view('account.buy-sell', compact('transactions'));
    }

    /**
     * @param User $username
     * @return Application|Factory|View
     */
    public function transactions(User $username)
    {
        $transactions = $username->transactions;

        return view('account.transactions', compact('transactions'));
    }

    public function accounts(User $username)
    {
        $user = $username->load('profile');
        $transactions = auth()->user()->transactions()->LATEST('created_at')->get();


        return view('account.accounts', compact('user', 'transactions'));
    }

    public function settings(User $username)
    {
        $user = $username;

        return view('account.settings', compact('user'));
    }

    /** update account details for users
     * @param Request $request
     * @param User $username
     * @return RedirectResponse
     */
    public function update(Request  $request, User $username)
    {
        switch ($request->input('form')) {
            case "profile":
                $this->userRepository->updateUserProfile($request, $username);
                session()->flash('success', "account updated successfully");
                return redirect()->back();
                break;
            case "password":
                $this->userRepository->changePassword($request, $username);
                session()->flash('success', "password Changed successfully");
                return redirect()->back();
                break;
            case "name":
                $this->userRepository->updateUserName($request, $username);
                session()->flash('success', "name changed successfully");
                return redirect()->back();
                break;
            default:
                return redirect()->back();
        }
    }

    public function settingsAccount(User $username)
    {
        $user = $username;

        return view('account.settings-account', compact('user'));
    }
}
