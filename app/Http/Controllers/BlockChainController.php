<?php

namespace App\Http\Controllers;

use App\Account;
use App\Modules\BlockChainModule;
use App\Transaction;
use http\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class BlockChainController extends Controller
{
    /**
     * @var BlockChainModule
     */
    private BlockChainModule $blockChain;

    public function __construct()
    {
        $this->blockChain = new BlockChainModule();
    }

    public function callback(Request $request)
    {
        Log::info($request);
        $transaction = Transaction::whereCode($request->input('code'))->firstOrFail();
        try {
            if ($this->blockChain->verifyPayment($request, $transaction) && $transaction->status == 'pending') {
                $transaction->verified = true;
                $transaction->status = 1;
                $transaction->save();
                $transaction->user->addBalance($transaction->amount);
            }

            echo "*ok*";
        } catch (Exception $e) {
            Log::error($e->getMessage());
            report($e);
        }
    }
}
