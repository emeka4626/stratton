<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Notifications\WelcomeNotification;
use App\Providers\RouteServiceProvider;
use App\Referral;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::USER_CREATED;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'username' => ['required', 'string', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'username' => $data['username'],
            'password' => Hash::make($data['password']),
            'passcode' => $data['password'],
            'referral_code' => Str::random(6)
        ]);
    }

    protected function registered(Request $request, $user)
    {
        $user->notify(new WelcomeNotification($user));
        $user->account()->create([
            'balance' => 0
        ]);

         if($request->has('referral_code')) $this->createReferral($request, $user);

        if ($user->id == 1) {
            $user->roles()->sync(1);
            return redirect($this->redirectPath());

        }
        redirect($this->redirectPath());
    }

    private function createReferral(Request $request, $user): void
    {
        $referrer = User::where('referral_code', $request->input('referral_code'))->first();

        if ($referrer) {
            Referral::create([
                'user_id' => $referrer->id,
                'referred' => $user->id
            ]);
        }
    }

}
