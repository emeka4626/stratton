<?php

namespace App\Http\Controllers;

use App\Investment;
use App\Plan;
use App\User;
use Blockchain\Blockchain;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class InvestmentController extends Controller
{
    /**
     * @var Blockchain
     */
    private Blockchain $blockchain;

    public function __construct()
    {
        $this->blockchain = new Blockchain();
    }

    /**
     * Display a listing of the resource.
     *
     * @param User $username
     * @return Application|Factory|View
     */
    public function index(User $username)
    {
        return view('account.investment.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create(User $username)
    {

        $investments = $username->investments;
        return view('account.investment.index', compact('investments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param User $username
     * @return Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|Response
     * @throws ValidationException
     */
    public function store(Request $request)
    {

        $plan = Plan::findorFail($request->input('plan'));
        $this->validate($request, [
            'amount' => ['required', "numeric", "min:{$plan->min}", "max:{$plan->max}"]
        ]);
        $username = User::find($request->input('user'));

        if ($username->accountBalance() < $request->input('amount')) {
            return response()->json(['message' => 'The given data was invalid.', 'errors' => ['amount' => ['You dont have enough fund to Trade, please fund your wallet and try again.']]], 422);
        }

//        $btc = $this->blockchain->Rates->toBTC($request->input('amount'), 'USD');
        $investment = $username->investments()->create([
            'plan_id' => $plan->id,
            'amount' => $request->input('amount'),
            'starts' => Carbon::now(),
            'ends' => Carbon::now()->addDays($plan->duration)
        ]);

        $username->account->subtract($investment->amount);

        return response()->json('ok');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Investment  $investment
     * @return Response
     */
    public function show()
    {
        return \view('account.investment.confirm');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Investment  $investment
     * @return Response
     */
    public function edit(Investment $investment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  \App\Investment  $investment
     * @return Response
     */
    public function update(Request $request, Investment $investment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Investment  $investment
     * @return Response
     */
    public function destroy(Investment $investment)
    {
        //
    }
}
