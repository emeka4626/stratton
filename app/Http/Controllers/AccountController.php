<?php

namespace App\Http\Controllers;

use App\Account;
use App\Notifications\CompleteAccountFunding;
use App\Plan;
use App\Repositories\TransactionRepository;
use App\Transaction;
use App\User;
use Blockchain\Blockchain;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class AccountController extends Controller
{
    private $transactionRepository;

    public function __construct(TransactionRepository $transactionRepository, Blockchain $blockchain)
    {
        $this->transactionRepository = $transactionRepository;
        $this->blockchain = $blockchain;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param User $username
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, User $username)
    {
        $this->validate($request, [
            'amount' => ['required'],
        ]);
        $volume = $this->blockchain->Rates->toBTC($request->input('amount'), 'USD');

        $code = uniqid(Str::random(10));
        $transaction = Transaction::create([
            'code' => $code,
            'amount' => $request->input('amount'),
            'value' => $volume,
            'status' => 'pending',
            'type' => 'wallet'
        ]);
        // attach user id to the transaction
        $transaction->user()->attach($username);

        /*
        * attach the cryptocurrency id to the transaction
        * default 1 is bitcoin
        * useful for adding multi currency support
        */
        $transaction->crypto()->attach(1);

        $username->notify(new CompleteAccountFunding($username));

        session()->flash('success', 'Wallet funding has been initiated');
        return redirect()->route('invest.confirm', $username)->with([
            'btc' => $volume
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function show(Account $account)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function edit(Account $account)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Account $account)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function destroy(Account $account)
    {
        //
    }
}
