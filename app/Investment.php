<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investment extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    protected $casts = [
        "starts" => "datetime",
        "ends" => "datetime",
    ];

    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function earning()
    {
        return $this->amount + ($this->amount * ($this->plan->percentage / 100));
    }
}
