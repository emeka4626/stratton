<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    //
    public function balance()
    {
        return $this->balance;
    }

    public function subtract($amount)
    {
        $this->balance -= $amount;
        $this->save();
    }
}
