<?php

namespace App\View\Components;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\View\Component;
use Illuminate\View\View;

class AdminHeader extends Component
{
    public ?Authenticatable $user;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->user = auth()->user();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
        $btcBalance = $this->user->btcBalance('complete');
        $usdBalance = $this->user->accountBalance();
        return view('components.admin-header', compact(['btcBalance', 'usdBalance']));
    }
}
