<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\View\View;

class Portfolio extends Component
{

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
        $user = auth()->user();

        $btcBalance = $user->btcBalance('complete');
        $usdBalance = $user->accountBalance();
        return view('components.portfolio', compact(['btcBalance', 'usdBalance']));
    }
}
