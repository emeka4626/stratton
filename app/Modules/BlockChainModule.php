<?php


namespace App\Modules;


use App\Transaction;
use Blockchain\Blockchain;
use Blockchain\Exception\Error;
use Blockchain\Exception\HttpError;
use Illuminate\Http\Request;

class BlockChainModule
{

    /**
     * The blockchain api key.
     *
     * @var array
     */
    protected $apiKey;

    /**
     * The blockchain xpub key.
     *
     * @var array
     */
    protected $xpubKey;

    /**
     * The blockchain secret key.
     *
     * @var array
     */
    protected $secret;

    /**
     * The blockchain instance.
     *
     * @var array
     */
    protected $blockchain;

    /**
     *  Class constructor.
     */
    public function __construct()
    {
        $this->apiKey = config('blockchain.api_key');
        $this->xpubKey = config('blockchain.xpub_key');
        $this->secret = 'ABCD!@#$HHHH^^%%@&%(';
        $this->blockchain = new Blockchain(config('blockchain.api_key'));
    }

    /**
     * Create blockchain payment.
     *
     * @param string $code
     * @param $amount
     * @return array
     * @throws Error
     * @throws HttpError
     */
    public function createPaymentAddress(string $code, $amount)
    {
        $response = $this->blockchain->ReceiveV2->generate($this->apiKey, $this->xpubKey, route('blockchain.callback', ['code' => $code, 'secret' => $this->secret]), 100);
        $volume = $this->usdToBtc($amount);
        return [
            'address' => $response->getReceiveAddress(),
            'amount' => $amount,
            'volume' => $volume,
        ];
    }

    /**
     * Verify blockchain payment.
     *
     * @param Request $request
     * @param Transaction $transaction
     * @return bool
     * @throws \Exception
     */
    public function verifyPayment(Request $request, Transaction $transaction)
    {
        if ($request->input('code') != $transaction->code) {
            throw new \Exception('Invalid payment address');
        }

        if (bcdiv($request->input('value'), 100000000, 8) != $transaction->volume) {
            throw new \Exception('Invalid payment amount');
        }

        if ($request->input('secret') != $this->secret) {
            throw new \Exception('Invalid payment secrete');
        }

        return $request->input('confirmations') >= 1;
    }

    /**
     * Get the exchange rate from btc to usd.
     *
     * @param $amount
     * @return int
     */
    public function btcToUsd($amount)
    {
        $rates = $this->blockchain->Rates->get();

        return $amount * $rates['USD']->m15;
    }

    /**
     * Get the exchange rate from btc to usd.
     *
     * @param $amount
     * @return int
     */
    public function usdToBtc($amount)
    {
        return $this->blockchain->Rates->toBTC($amount, 'USD');
    }
}
