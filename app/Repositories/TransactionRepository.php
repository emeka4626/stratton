<?php


namespace App\Repositories;


use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TransactionRepository
{

    public function createTransaction(Request $data, $reference, $status = 'pending', $type = "trade"): Transaction
    {
        return Transaction::create([
           'code' => $reference,
           'amount' => $data->input('amount'),
           'value' => $data->input('volume'),
            'status' => $status,
            'type' => $type
        ]);
    }
}
