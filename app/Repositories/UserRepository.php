<?php


namespace App\Repositories;


use App\User;
use Illuminate\Http\Request;

class UserRepository
{
    public function updateUserName(Request $request, User $user): bool
    {
        return $user->update([
            'name' => $request->input('name')
        ]);
    }

    public function changePassword(Request $request, User $user)
    {
        return $user->update([
            'password' => bcrypt($request->input('password'))
        ]);
    }

    public function updateUserProfile(Request $request, User $user)
    {
         collect($request->all())->each(function ($value, $field) use ($user) {
            if (!collect(['form', '_token'])->contains($field)) {
                $user->profile()->update([
                    $field => $value
                ]);
            }
        });
    }
}
