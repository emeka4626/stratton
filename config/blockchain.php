<?php

return [
    'payment_currency' => env('BLOCKCHAIN_PAYMENT_CURRENCY', 'USD'),
    'receive_currency' => env('BLOCKCHAIN_RECEIVE_CURRENCY', 'USD'),
    'api_key' => env('BLOCKCHAIN_API_KEY'),
    'xpub_key' => env('BLOCKCHAIN_XPUB_KEY'),
];
