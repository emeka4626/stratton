import axios from 'axios';

 export default class CurrencyService {

    constructor() {
        this.apiKey = '081629221b35dedfd519becf83c7b624';
        this.apiUrl = 'https://api.nomics.com/v1';
    }

     async market() {
         let requestOptions = {
             method: 'GET',
             redirect: 'follow'
         };
         return await fetch(`${this.apiUrl}/currencies/ticker?key=${this.apiKey}&ids=BTC&interval=1h&convert=USD`, requestOptions)
             .then(response => response.text())
             .then(result => result)
             .catch(error => console.log('error', error));
     }
}
