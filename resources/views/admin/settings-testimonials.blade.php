@extends('scaffold.main')

@section('top-section')
    <x-admin-header></x-admin-header>
    <x-admin-sidebar></x-admin-sidebar>
@endsection

@section('body')

    <div class="content-body">
        <div class="verification">
            <div class="container">
                <div class="row justify-content-center align-items-center  my-5">
                    <div class="col-xl-5 col-md-6">
                        <div class="auth-form card">
                            <div class="card-header">
                                <h4 class="card-title">Add Testimonial</h4>
                            </div>
                            <div class="card-body">
                                <form action="{{ route('admin.settings.testimonial.save', auth()->user()) }}" method="POST" class="identity-upload" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-row">
                                        <div class="form-group col-xl-12">
                                            <label class="mr-sm-2">Customer Name </label>
                                            <input type="text" class="form-control @error('name') 'is-invalid' @enderror" placeholder="John Doe" name="name">
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-xl-12">
                                            <label class="mr-sm-2">Description </label>
                                            <textarea type="number" class="form-control @error('text') 'is-invalid' @enderror" placeholder="Enter Description Here" name="text"></textarea>
                                            @error('text')
                                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ __("Description is required") }}</strong>
                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-xl-12">
                                            <label class="mr-sm-2">Company </label>
                                            <input type="text" class="form-control @error('company') 'is-invalid' @enderror" placeholder="CEO, Shell Corp" name="company">
                                            @error('company')
                                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ __("Company is required") }}</strong>
                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-xl-12">
                                            <label class="mr-sm-2">Upload Image </label>
                                            <span class="float-right">Maximum file size is 2mb</span>
                                            <div class="file-upload-wrapper" data-text="front image">
                                                <input name="image" type="file" class="file-upload-field">
                                                @error('image')
                                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ __("a valid image is required") }}</strong>
                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="text-center col-12">
                                            <a href="{{ route('admin.settings', auth()->user()) }}" class="btn btn-primary mx-2">Back</a>
                                            <button type="submit" class="btn btn-success mx-2">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-scripts')

@endsection
