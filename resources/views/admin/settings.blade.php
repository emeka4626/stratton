@extends('scaffold.main')

@section('top-section')
    <x-admin-header></x-admin-header>
    <x-admin-sidebar></x-admin-sidebar>
@endsection

@section('body')
    <div class="content-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-3 col-md-4">
                    <div class="card settings_menu">
                        <div class="card-header">
                            <h4 class="card-title">Options</h4>
                        </div>
                        <div class="card-body">
                            <ul>
                                <li class="nav-item">
                                    <a href="{{ route('admin.settings.plans', auth()->user()) }}" class="nav-link active">
                                        <i class="la la-file-excel"></i>
                                        <span>Investment Plans</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin.settings.testimonials', auth()->user()) }}" class="nav-link active">
                                        <i class="la la-people-carry"></i>
                                        <span>Add Testimonial</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-9 col-lg-9 col-xxl-9">
                    <div class="card profile_chart">
                        <div class="card-body">
                            <div id="timeline-chart"></div>
                            <div class="chart-content text-center">
                                <div class="pb-4">
                                    <h4 class="card-title">Settings Overview</h4>
                                </div>
                                <div class="row">
                                    <div class="col-xl-6 col-sm-12 col-12">
                                        <div class="chart-stat">
                                            <p class="mb-1">Investment Plan</p>
                                            <div class="d-flex justify-content-center align-items-center">
                                                <div class="icon mr-4"><i class="la la-3x la-wallet"></i></div>
                                                <h5>{{ $plans }}</h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-sm-12 col-12">
                                        <div class="chart-stat">
                                            <p class="mb-1">User Testimonials</p>
                                            <div class="d-flex justify-content-center align-items-center">
                                                <div class="icon mr-4"><i class="la la-3x la-people-carry"></i></div>
                                                <h5>{{ $testimonials }}</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('extra-scripts')

@endsection

