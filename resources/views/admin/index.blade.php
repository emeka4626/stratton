@extends('scaffold.admin')

@section('top-section')
    <x-admin-header></x-admin-header>
    <x-admin-sidebar></x-admin-sidebar>
@endsection
@section('body')
    <div class="content-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-10 col-lg-10 col-xxl-10">
                    <div class="card profile_chart">
                        <div class="card-body">
                            <div id="timeline-chart"></div>
                            <div class="chart-content text-center">
                                <div class="pb-4">
                                    <h4 class="card-title">TradeStone Overview</h4>
                                </div>
                                <div class="row">
                                    <div class="col-xl-4 col-sm-12 col-12">
                                        <div class="chart-stat">
                                            <p class="mb-1">Total Bitcoin purchase</p>
                                            <div class="d-flex align-items-center">
                                                <div class="icon mr-4"><i class="la la-3x la-wallet"></i></div>
                                                <h5>${{ number_format($transactions) }}</h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-sm-12 col-12">
                                        <div class="chart-stat">
                                            <p class="mb-1">Total Investment Deposit</p>
                                            <div class="d-flex align-items-center">
                                                <div class="icon mr-4"><i class="la la-3x la-wallet"></i></div>
                                                <h5>${{ number_format($investments) }}</h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-sm-12 col-12">
                                        <div class="chart-stat">
                                            <p class="mb-1">Total Balance</p>
                                            <div class="d-flex align-items-center">
                                                <div class="icon mr-4"><i class="la la-3x la-money-bill-wave"></i></div>
                                                <h5>${{ number_format($balance) }}</h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-sm-12 col-12">
                                        <div class="chart-stat">
                                            <p class="mb-1">Total Users</p>
                                            <div class="d-flex align-items-center">
                                                <div class="icon mr-4"><i class="la la-3x la-users"></i></div>
                                                <h5>{{ number_format($users) }}</h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-sm-12 col-12">
                                        <div class="chart-stat">
                                            <p class="mb-1">Total Withdrawals</p>
                                            <div class="d-flex align-items-center">
                                                <div class="icon mr-4"><i class="la la-3x la-wallet"></i></div>
                                                <h5>${{ number_format($withdrawals) }}</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-scripts')

@endsection
