@extends('scaffold.main')

@section('top-section')
    <x-admin-header></x-admin-header>
    <x-admin-sidebar></x-admin-sidebar>
@endsection

@section('body')
    <div class="content-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-header border-0">
                            <h4 class="card-title">All Activities</h4>
                        </div>
                        <div class="card-body pt-0">
                            <div class="transaction-table">
                                <div class="table-responsive">
                                    <table class="table mb-0 table-responsive-sm">
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Username</th>
                                            <th>Amount</th>
                                            <th>Wallet address</th>
                                            <th>Date Requested</th>
                                        </tr>
                                        <tbody>
                                        @foreach($withdrawals as $key => $withdrawal)
                                            <tr>
                                                <td>
                                                    <span class="buy-thumb @if($withdrawal->status == 0) bg-warning @else bg-success @endif">
                                                        {{ $key + 1 }}
                                                    </span>
                                                </td>
                                                <td>
                                                    {{ $withdrawal->user->name() }}
                                                </td>
                                                <td>
                                                    {{ $withdrawal->user->username }}
                                                </td>
                                                <td class="@if($withdrawal->status == 0) text-warning @else text-success @endif">+{{ $withdrawal->amount }} USD</td>
                                                <td>{{ $withdrawal->wallet }}</td>
                                                <td>{{ $withdrawal->created_at->diffForHumans() }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('extra-scripts')

@endsection
