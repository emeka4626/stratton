@extends('scaffold.main')

@section('top-section')
    <x-admin-header></x-admin-header>
    <x-admin-sidebar></x-admin-sidebar>
@endsection

@section('body')
    <div class="content-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-3 col-md-4">
                    <div class="card settings_menu">
                        <div class="card-header">
                            <h4 class="card-title">Options</h4>
                        </div>
                        <div class="card-body">
                            <ul>
                                <li class="nav-item">
                                    <a href="{{ route('admin.investments.addFund', auth()->user()) }}" class="nav-link active">
                                        <i class="la la-money-bill-wave-alt"></i>
                                        <span>Add Fund</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin.investments.pending', auth()->user()) }}" class="nav-link active">
                                        <i class="la la-exchange"></i>
                                        <span>Pending Investments</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin.investments.confirmed', auth()->user()) }}" class="nav-link">
                                        <i class="la la-exchange-alt"></i>
                                        <span>Completed Investments</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-9 col-md-8">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row h-100  my-5">
                                <div class="col-xl-6 col-md-6">
                                    <div class="auth-form card">
                                        <div class="card-header">
                                            <h4 class="card-title">Fund User Account</h4>
                                        </div>
                                        <div class="card-body">
                                            <form action="{{ route('admin.investments.fund', auth()->user()) }}" method="POST" class="identity-upload">
                                                @csrf
                                                <div class="form-row">
                                                    <div class="form-group col-xl-12">
                                                        <label class="mr-sm-2" for="username">Username  </label>
                                                        <input type="text" id="username" class="form-control @error('username') 'is-invalid' @enderror" placeholder="Username" name="username">
                                                        @error('username')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group col-xl-12">
                                                        <label for="plan" class="mr-sm-2">Choose Investment Plan</label>
                                                        <select id="plan" class="form-control @error('plan') is-invalid @enderror" name="plan">
                                                            <option value="">Select</option>
                                                            <option value="1">$50 - $150 (5%)</option>
                                                            <option value="2">$200 - $900  (20%)</option>
                                                            <option value="3">$1,000 - $5,000 (40%)</option>
                                                            <option value="4">$6,000$ - $10,000 (60%)</option>
                                                            <option value="5">$11,000 - $15,000 (80%)</option>
                                                            <option value="6">$16,000 - $20,000 (100%)</option>

                                                        </select>
                                                        @error('plan')
                                                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group col-xl-12">
                                                        <label class="mr-sm-2" for="amount">Amount </label>
                                                        <input type="text" id="amount" class="form-control @error('amount') 'is-invalid' @enderror" placeholder="500" name="amount">
                                                        @error('amount')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                    <div class="text-center col-12">
                                                        <button type="submit" class="btn btn-success mx-2">Fund Wallet</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-scripts')

@endsection

