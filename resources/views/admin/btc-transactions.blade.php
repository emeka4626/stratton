@extends('scaffold.main')

@section('top-section')
    <x-admin-header></x-admin-header>
    <x-admin-sidebar></x-admin-sidebar>
@endsection

@section('body')
    <div class="content-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-3 col-md-4">
                    <div class="card settings_menu">
                        <div class="card-header">
                            <h4 class="card-title">Options</h4>
                        </div>
                        <div class="card-body">
                            <ul>
                                <li class="nav-item">
                                    <a href="{{ route('admin.fundWallet', auth()->user()) }}" class="nav-link active">
                                        <i class="la la-wallet"></i>
                                        <span>Fund Wallet</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin.pendingTransactions', auth()->user()) }}" class="nav-link active">
                                        <i class="la la-exchange"></i>
                                        <span>Pending Transactions</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin.completedTransactions', auth()->user()) }}" class="nav-link">
                                        <i class="la la-exchange-alt"></i>
                                        <span>Completed Transactions</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-9 col-md-8">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{ $page }}</h4>
                                </div>
                                <div class="card-body">
                                    <div class="col-xl-12 col-lg-12 col-md-12">
                                        <div class="card">
                                            <div class="card-header border-0 py-0">
                                                <h4 class="card-title">Activities</h4>
                                                <a href="#">More </a>
                                            </div>
                                            <div class="card-body">
                                                <div class="market-table">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0 table-responsive-sm">
                                                            <tr>
                                                                <th>#</th>
                                                                <th colspan="2">Name</th>
{{--                                                                <th></th>--}}
                                                                <th>Currency</th>
                                                                <th>Value</th>
                                                                <th>Amount</th>
                                                                <th>Date</th>
                                                                <th>Action</th>
                                                            </tr>
                                                            <tbody>
                                                                @foreach($transactions as $key => $transaction)
                                                                    <tr>
                                                                        <td>{{ $key + 1 }}</td>
                                                                        <td colspan="2">
                                                                            {{ $transaction->getUser()->name() }}
                                                                        </td>
{{--                                                                        <td>--}}
{{--                                                                            <span class="badge @if($transaction->status == 'pending') badge-warning @else badge-success @endif" >Buy</span>--}}
{{--                                                                        </td>--}}
                                                                        <td>
                                                                            <i class="cc BTC"></i> Bitcoin
                                                                        </td>
                                                                        <td class="@if($transaction->status == 'pending') text-warning @else text-success @endif">+{{ $transaction->value }} BTC</td>
                                                                        <td>+{{ $transaction->amount }} USD</td>
                                                                        <td>{{ $transaction->created_at->diffForHumans() }}</td>
                                                                        @if($transaction->status === 'pending')
                                                                            <td>
                                                                                <a href="{{ route('admin.transaction.confirm', [auth()->user(), $transaction]) }}" class="btn btn-success">
                                                                                    Confirm
                                                                                </a>
                                                                            </td>

                                                                        @else
                                                                            <td>
                                                                                <a class="btn btn-outline-success disabled">
                                                                                    Confirmed
                                                                                </a>
                                                                            </td>
                                                                        @endif
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-scripts')

@endsection

