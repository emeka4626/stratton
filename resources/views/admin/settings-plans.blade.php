@extends('scaffold.main')

@section('top-section')
    <x-admin-header></x-admin-header>
    <x-admin-sidebar></x-admin-sidebar>
@endsection

@section('body')
    <div class="content-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-3 col-md-4">
                    <div class="card settings_menu">
                        <div class="card-header">
                            <h4 class="card-title">Options</h4>
                        </div>
                        <div class="card-body">
                            <ul>
                                <li class="nav-item">
                                    <a href="{{ route('admin.settings.plans', auth()->user()) }}"
                                       class="nav-link active">
                                        <i class="la la-file-excel"></i>
                                        <span>Investment Plans</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin.settings.testimonials', auth()->user()) }}"
                                       class="nav-link active">
                                        <i class="la la-people-carry"></i>
                                        <span>Testimonials</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-9 col-md-8">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="card">

                                <div class="card-header">
                                    <h4 class="card-title">Investment Plans</h4>
                                    <a href="{{ route('admin.settings.plans.create', auth()->user()) }}">Add Plan </a>
                                </div>
                                <div class="card-body">
                                    <div class="col-xl-12 col-lg-12 col-md-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="transaction-table">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0 table-responsive-sm">
                                                            <tr>
                                                                <th>Name</th>
                                                                <th>Duration</th>
                                                                <th>Percentage Return</th>
                                                                <th>Minimum Deposit</th>
                                                                <th>Maximum Deposit</th>
                                                            </tr>
                                                            <tbody>
                                                            @foreach($plans as $plan)
                                                                <tr>
                                                                    <td class="text-center">
                                                                        {{ ucwords($plan->name) }}
                                                                    </td>
                                                                    <td class="text-center">
                                                                        {{ $plan->duration }} Days
                                                                    </td>
                                                                    <td class="text-center">
                                                                        {{ $plan->percentage }}
                                                                    </td>

                                                                    <td class="text-center">
                                                                        {{ $plan->min }}
                                                                    </td>
                                                                    <td class="text-center">
                                                                        {{ $plan->max }}
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('extra-scripts')

@endsection

