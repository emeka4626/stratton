@extends('scaffold.main')

@section('top-section')
    <x-admin-header></x-admin-header>
    <x-admin-sidebar></x-admin-sidebar>
@endsection

@section('body')

    <div class="content-body">
        <div class="verification">
            <div class="container">
                <div class="row justify-content-center align-items-center  my-5">
                    <div class="col-xl-5 col-md-6">
                        <div class="auth-form card">
                            <div class="card-header">
                                <h4 class="card-title">Create Plan</h4>
                            </div>
                            <div class="card-body">
                                <form action="{{ route('admin.settings.plans.save', auth()->user()) }}" method="POST" class="identity-upload">
                                    @csrf
                                    <div class="form-row">
                                        <div class="form-group col-xl-12">
                                            <label class="mr-sm-2">Plan Name </label>
                                            <input type="text" class="form-control @error('name') 'is-invalid' @enderror" placeholder="Starter Plan" name="name">
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-xl-12">
                                            <label class="mr-sm-2">Duration </label>
                                            <input type="number" class="form-control @error('duration') 'is-invalid' @enderror" placeholder="1" name="duration">
                                            @error('duration')
                                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ __("Plan duration is required") }}</strong>
                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-xl-12">
                                            <label class="mr-sm-2">Percentage Return </label>
                                            <input type="text" class="form-control @error('percentage') 'is-invalid' @enderror" placeholder="10" name="percentage">
                                            @error('percentage')
                                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ __("Percentage is required") }}</strong>
                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-xl-6">
                                            <label class="mr-sm-2">Minimum Investment </label>
                                            <input type="text" class="form-control @error('min') 'is-invalid' @enderror" placeholder="10" name="min">
                                            @error('min')
                                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ __("Minimum Investment is required") }}</strong>
                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-xl-6">
                                            <label class="mr-sm-2">Maximum Investment </label>
                                            <input type="text" class="form-control @error('max') 'is-invalid' @enderror" placeholder="10" name="max">
                                            @error('max')
                                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ __("Maximum investment is required") }}</strong>
                                </span>
                                            @enderror
                                        </div>
                                        <div class="text-center col-12">
                                            <a href="{{ route('admin.settings.plans', auth()->user()) }}" class="btn btn-primary mx-2">Back</a>
                                            <button type="submit" class="btn btn-success mx-2">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-scripts')

@endsection
