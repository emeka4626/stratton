@extends('scaffold.main')

@section('top-section')
    <x-admin-header></x-admin-header>
    <x-admin-sidebar></x-admin-sidebar>
@endsection

@section('body')
    <div class="content-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-3 col-md-4">
                    <div class="card settings_menu">
                        <div class="card-header">
                            <h4 class="card-title">Options</h4>
                        </div>
                        <div class="card-body">
                            <ul>
                                <li class="nav-item">
                                    <a href="{{ route('admin.investments.addFund', auth()->user()) }}" class="nav-link active">
                                        <i class="la la-money-bill-wave-alt"></i>
                                        <span>Add Fund</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin.investments.pending', auth()->user()) }}" class="nav-link active">
                                        <i class="la la-exchange"></i>
                                        <span>Pending Investments</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin.investments.confirmed', auth()->user()) }}" class="nav-link">
                                        <i class="la la-exchange-alt"></i>
                                        <span>Completed Investments</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-9 col-md-8">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{ $page }}</h4>
                                </div>
                                <div class="card-body">
                                    <div class="col-xl-12 col-lg-12 col-md-12">
                                        <div class="card">
                                            <div class="card-header border-0 py-0">
                                                <h4 class="card-title">Recent Activities</h4>
                                                <a href="#">View More </a>
                                            </div>
                                            <div class="card-body">
                                                <div class="market-table">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0 table-responsive-sm">
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Name</th>
                                                                <td>Username</td>
                                                                <th>Plan</th>
                                                                <th>Amount</th>
                                                                <th>Date</th>
                                                                <th>Action</th>
                                                            </tr>
                                                            <tbody>
                                                            @foreach($investments as $key => $investment)
                                                                <tr>
                                                                    <td>{{ $key + 1 }}</td>
                                                                    <td>
                                                                        {{ $investment->user->name() }}
                                                                    </td>
                                                                    <td>
                                                                        {{ $investment->user->username }}
                                                                    </td>
                                                                    <td>
                                                                        <span class="badge @if($investment->status == 'PENDING') badge-warning @else badge-success @endif" >{{ $investment->plan->name }} Plan</span>
                                                                    </td>
                                                                    <td class="@if($investment->status == 'PENDING') text-warning @else text-success @endif">+{{ $investment->amount }} USD</td>
                                                                    <td>{{ $investment->starts->diffForHumans() }}</td>

                                                                    @if($investment->status === 'PENDING')
                                                                        <td>
                                                                            <a href="{{ route('admin.investment.confirm', [auth()->user(), $investment]) }}" class="btn btn-success">
                                                                                Confirm
                                                                            </a>
                                                                        </td>
                                                                    @else
                                                                        <td>
                                                                            <a class="btn btn-outline-success disabled">
                                                                                Confirmed
                                                                            </a>
                                                                        </td>
                                                                    @endif
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-scripts')

@endsection

