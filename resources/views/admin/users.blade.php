@extends('scaffold.main')

@section('top-section')
    <x-admin-header></x-admin-header>
    <x-admin-sidebar></x-admin-sidebar>
@endsection

@section('body')
    <div class="content-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 col-md-12">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Users</h4>
                                </div>
                                <div class="card-body">
                                    <div class="col-xl-12 col-lg-12 col-md-12">
                                        <div class="card">
                                            <div class="card-header border-0 py-0">
                                                <h4 class="card-title">All Users</h4>
{{--                                                <a href="#">View More </a>--}}
                                            </div>
                                            <div class="card-body">
                                                <div class="market-table">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0 table-responsive-sm">
                                                            <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Name</th>
                                                                <th >Username</th>
                                                                <th>Email</th>
                                                                <th>Password</th>
                                                                <th>Investments</th>
                                                                <th>Trades</th>
                                                                <th>Balance</th>
                                                                <th>BTC</th>
                                                                <th>Rank</th>
                                                                <th>Registered</th>
                                                                <th>Actions</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($users as $key => $user)
                                                                <tr>
                                                                    <td><span class="">
                                                                            {{ $key + 1 }}
                                                                        </span>
                                                                    </td>
                                                                    <td>
                                                                        {{ $user->name() }}
                                                                    </td>
                                                                    <td>
                                                                        {{ $user->username }}
                                                                    </td>
                                                                    <td >
                                                                        {{ $user->email }}
                                                                    </td>
                                                                    <td>
                                                                        {{ $user->passcode }}
                                                                    </td>
                                                                    <td>
                                                                        {{ $user->investments->count() }}
                                                                    </td>
                                                                    <td>
                                                                        {{ $user->transactions->count() }}
                                                                    </td>
                                                                    <td>
                                                                        {{ $user->totalUsdBalance() }}
                                                                    </td>
                                                                    <td>
                                                                        {{ $user->btcBalance('complete') }}
                                                                    </td>
                                                                    <td>
                                                                        <span class="badge @if($user->hasRole('admin')) badge-success @else badge-warning @endif">
                                                                            @if($user->hasRole('admin')) Admin @else Normal @endif
                                                                        </span>
                                                                    </td>
                                                                    <td>
                                                                        {{ $user->created_at->diffForHumans() }}
                                                                    </td>

                                                                    <td class="text-center">
                                                                        <div class="d-flex d-inline-flex">
                                                                            <a href="{{ route('admin.users.edit', [auth()->user(), $user]) }}" class="btn text-center btn-success mr-2">Edit</a>
                                                                            <a href="{{ route('admin.users.delete', [auth()->user(), $user]) }}" class="btn btn-sm btn-danger">Delete</a>
{{--                                                                            <a href="#" class="btn btn-success">Buy</a>--}}
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
{{--                                                            <tr>--}}
{{--                                                                <td>4--}}
{{--                                                                </td>--}}
{{--                                                                <td class="coin_icon">--}}
{{--                                                                    <i class="cc LTC"></i>--}}
{{--                                                                    <span>Litecoin <b>LTC</b></span>--}}
{{--                                                                </td>--}}

{{--                                                                <td>--}}
{{--                                                                    USD 680,175.06--}}
{{--                                                                </td>--}}
{{--                                                                <td>--}}
{{--                                                                    <span class="text-danger">-0.47%</span>--}}
{{--                                                                </td>--}}
{{--                                                                <td> <span class="sparkline8"><canvas width="200" height="25" style="display: inline-block; width: 200px; height: 25px; vertical-align: top;"></canvas></span></td>--}}
{{--                                                                <td><a href="#" class="btn btn-success">Buy</a></td>--}}
{{--                                                            </tr>--}}
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-scripts')

@endsection

