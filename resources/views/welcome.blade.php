@extends('scaffold.base')


@section('body')
    @include('partials.header')
    <div class="intro">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-xl-6 col-lg-6 col-12">
                    <div class="intro-content">
                        <h1>Trade with <strong class="text-primary">StrattonTrade</strong>. <br> Buy and sell
                            cryptocurrency
                        </h1>
                        <p>Fast and secure way to purchase or exchange 150+ cryptocurrencies</p>
                    </div>

                    <div class="intro-btn">
                        <a href="{{ route('register') }}" class="btn btn-primary">Get Started</a>
                    </div>
                </div>
                <div class="col-xl-5 col-lg-6 col-12">
                    <div class="section-padding">
                        <div class="container">
                            <div class="testimonial-content">
                                <div class="testimonial1 owl-carousel owl-theme">
                                    <div class="row align-items-center">
                                        <div class="col-xl-6 col-lg-6">
                                            <div class="customer-img">
                                                <img class="h-full w-100"
                                                     src="https://www.finextra.com/finextra-images/oped/40.jpeg" alt="">
                                            </div>
                                        </div>
                                        <div class="col-xl-6 col-lg-6">
                                            <div class="customer-review">
                                                {{--                                        <img class="img-fluid" src="images/brand/2.webp" alt="">--}}
                                                <p>You choose what to Invest on and exit a trade when you are satisfied
                                                    with your
                                                    accumulated profits anytime.</p>
                                                <div class="customer-info">
                                                    <h6>Invest from your<br> comfort zone</h6>
                                                    <p>We hope success</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row align-items-center">
                                        <div class="col-xl-6 col-lg-6">
                                            <div class="customer-img">
                                                <img class="h-full w-100"
                                                     src="http://www.traderzone4crypto.com/landing-asset/images/main-slider/7.jpg"
                                                     alt="">
                                            </div>
                                        </div>
                                        <div class="col-xl-6 col-lg-6">
                                            <div class="customer-review">
                                                {{--                                        <img class="img-fluid" src="images/brand/2.webp" alt="">--}}
                                                <p>You choose what to Invest on and exit a trade when you are satisfied
                                                    with your
                                                    accumulated profits anytime.</p>
                                                <div class="customer-info">
                                                    <h6>Invest from your<br> comfort zone</h6>
                                                    <p>We hope success</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row align-items-center">
                                        <div class="col-xl-6 col-lg-6">
                                            <div class="customer-img">
                                                <img class="h-full w-100"
                                                     src="http://www.traderzone4crypto.com/landing-asset/images/main-slider/6.jpg"
                                                     alt="">
                                            </div>
                                        </div>
                                        <div class="col-xl-6 col-lg-6">
                                            <div class="customer-review">
                                                {{--                                        <img class="img-fluid" src="images/brand/2.webp" alt="">--}}
                                                <p>You choose what to Invest on and exit a trade when you are satisfied
                                                    with your
                                                    accumulated profits anytime.</p>
                                                <div class="customer-info">
                                                    <h6>Invest from your<br> comfort zone</h6>
                                                    <p>We hope success</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="price-grid section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-md-12">
                    <div
                        style="height:560px; background-color: #FFFFFF; overflow:hidden; box-sizing: border-box; border: 1px solid #56667F; border-radius: 4px; text-align: right; line-height:14px; font-size: 12px; font-feature-settings: normal; text-size-adjust: 100%; box-shadow: inset 0 -20px 0 0 #56667F;padding:1px;padding: 0px; margin: 0px; width: 100%;">
                        <div style="height:540px; padding:0px; margin:0px; width: 100%;">
                            <iframe
                                src="https://widget.coinlib.io/widget?type=chart&theme=light&coin_id=859&pref_coin_id=1505"
                                width="100%" height="536px" scrolling="auto" marginwidth="0" marginheight="0"
                                frameborder="0" border="0"
                                style="border:0;margin:0;padding:0;line-height:14px;"></iframe>
                        </div>
                        <div
                            style="color: #FFFFFF; line-height: 14px; font-weight: 400; font-size: 11px; box-sizing: border-box; padding: 2px 6px; width: 100%; font-family: Verdana, Tahoma, Arial, sans-serif;">
                            <a href="https://coinlib.io" target="_blank"
                               style="font-weight: 500; color: #FFFFFF; text-decoration:none; font-size:11px">Cryptocurrency
                                Prices</a>&nbsp;by Coinlib
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="getstart section-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8">
                    <div class="section-title">
                        <h2>Get started in a few minutes</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                    <div class="getstart-content">
                        <span><i class="la la-user-plus"></i></span>
                        <h3>Create an account</h3>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                    <div class="getstart-content">
                        <span><i class="la la-bank"></i></span>
                        <h3>Link your bank account</h3>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                    <div class="getstart-content">
                        <span><i class="la la-exchange"></i></span>
                        <h3>Start buying & selling</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="portfolio section-padding" data-scroll-index="2">
        <div class="container">
            <div class="row py-lg-5 justify-content-center">
                <div class="col-xl-7">
                    <div class="section-title text-center">
                        <h2>Create your cryptocurrency portfolio today</h2>
                        <p>StrattonTrade has a variety of features that make it the best place to start trading</p>
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-xl-12">
                    <div class="portfolio_list">
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="media">
                                    <span class="port-icon"> <i class="la la-bar-chart"></i></span>
                                    <div class="media-body">
                                        <h4>Manage your portfolio</h4>
                                        <p>Buy and sell popular digital currencies, keep track of them in the one
                                            place.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="media">
                                    <span class="port-icon"> <i class="la la-calendar-check-o"></i></span>
                                    <div class="media-body">
                                        <h4>Recurring buys</h4>
                                        <p>Invest in cryptocurrency slowly over time by scheduling buys daily,
                                            weekly,
                                            or monthly.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="media">
                                    <span class="port-icon"> <i class="la la-lock"></i></span>
                                    <div class="media-body">
                                        <h4>Vault protection</h4>
                                        <p>For added security, store your funds in a vault with time delayed
                                            withdrawals.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="media">
                                    <span class="port-icon"> <i class="la la-mobile"></i></span>
                                    <div class="media-body">
                                        <h4>Mobile apps</h4>
                                        <p>Stay on top of the markets with the StrattonTrade app for <a
                                                href="#">Android</a>
                                            or
                                            <a href="#">iOS</a>.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
{{--                <div class="col-xl-5 col-lg-6">--}}
{{--                    <div class="portfolio_img">--}}
{{--                        <img src="images/portfolio.png" alt="" class="img-fluid">--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
        </div>
    </div>

    <div class="trade-app section-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6">
                    <div class="section-title text-center">
                        <h2>Trade. Anywhere</h2>
                        <p> All of our products are ready to go, easy to use and offer great value to any kind of
                            business
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-12">
                    <div class="card trade-app-content">
                        <div class="card-body">
                            <span><i class="la la-mobile"></i></span>
                            <h4 class="card-title">Mobile</h4>
                            <p>All the power of StrattonTrade's cryptocurrency exchange, in the palm of your hand. Download
                                the
                                StrattonTrade mobile crypto trading app today</p>

                            <a href="#"> Know More <i class="la la-arrow-right"></i> </a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-12">
                    <div class="card trade-app-content">
                        <div class="card-body">
                            <span><i class="la la-desktop"></i></span>
                            <h4 class="card-title">Desktop</h4>
                            <p>Powerful crypto trading platform for those who mean business. The StrattonTrade crypto
                                trading
                                experience, tailor-made for your Windows or MacOS device.</p>

                            <a href="#"> Know More <i class="la la-arrow-right"></i> </a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-12">
                    <div class="card trade-app-content">
                        <div class="card-body">
                            <span><i class="la la-connectdevelop"></i></span>
                            <h4 class="card-title">API</h4>
                            <p>The StrattonTrade API is designed to provide an easy and efficient way to integrate your
                                trading
                                application into our platform.</p>

                            <a href="#"> Know More <i class="la la-arrow-right"></i> </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pt-5">
                <div class="col-xl-12">
                    <div class="trusted-business py-5 text-center">
                        <h3>Trusted by Our <strong>Partners & Investors</strong></h3>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-auto">
                            <div class="trusted-logo">
                                <a href="#"><img class="img-fluid" src="images/brand/1.webp" alt=""></a>
                            </div>
                        </div>
                        <div class="col-auto">
                            <div class="trusted-logo">
                                <a href="#"><img class="img-fluid" src="images/brand/2.webp" alt=""></a>
                            </div>
                        </div>
                        <div class="col-auto">
                            <div class="trusted-logo">
                                <a href="#"><img class="img-fluid" src="images/brand/3.webp" alt=""></a>
                            </div>
                        </div>
                        <div class="col-auto">
                            <div class="trusted-logo">
                                <a href="#"><img class="img-fluid" src="images/brand/4.webp" alt=""></a>
                            </div>
                        </div>
                        <div class="col-auto">
                            <div class="trusted-logo">
                                <a href="#"><img class="img-fluid" src="images/brand/5.webp" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="testimonial section-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6">
                    <div class="section-title">
                        <h2>What our customer says</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xl-10">
                    <div class="testimonial-content">
                        <div class="testimonial1 owl-carousel owl-theme">
                            @foreach($testimonials as $testimonial)
                                <div class="row align-items-center">
                                    <div class="col-xl-6 col-lg-6">
                                        <div class="customer-img">
                                            <img class="img-fluid"
                                                 src="{{ asset('uploads/testimonials/'.$testimonial->image) }}" alt="">
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-6">
                                        <div class="customer-review">
                                            {{--                                        <img class="img-fluid" src="images/brand/2.webp" alt="">--}}
                                            <p>{{ $testimonial->text }}</p>
                                            <div class="customer-info">
                                                <h6>{{ $testimonial->name }}</h6>
                                                <p>{{ $testimonial->company }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="promo section-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8">
                    <div class="section-title text-center">
                        <h2>The most trusted cryptocurrency platform</h2>
                        <p> Here are a few reasons why you should choose StrattonTrade
                        </p>
                    </div>
                </div>
            </div>
            <div class="row align-items-center py-5">
                <div class="col-xl-4 col-lg-4 col-md-4">
                    <div class="promo-content">
                        <div class="promo-content-img">
                            <img class="img-fluid" src="images/svg/protect.svg" alt="">
                        </div>
                        <h3>Secure storage </h3>
                        <p>We store the vast majority of the digital assets in secure offline storage.</p>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4">
                    <div class="promo-content">
                        <div class="promo-content-img">
                            <img class="img-fluid" src="images/svg/cyber.svg" alt="">
                        </div>
                        <h3>Protected by insurance</h3>
                        <p>Cryptocurrency stored on our servers is covered by our insurance policy.</p>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4">
                    <div class="promo-content">
                        <div class="promo-content-img">
                            <img class="img-fluid" src="images/svg/finance.svg" alt="">
                        </div>
                        <h3>Industry best practices</h3>
                        <p>StrattonTrade supports a variety of the most popular digital currencies.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="get-touch section-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6">
                    <div class="section-title">
                        <h2>Get in touch. Stay in touch.</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="get-touch-content">
                        <div class="media">
                            <span><i class="fa fa-shield"></i></span>
                            <div class="media-body">
                                <h4>24 / 7 Support</h4>
                                <p>Got a problem? Just get in touch. Our support team is available 24/7.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="get-touch-content">
                        <div class="media">
                            <span><i class="fa fa-cubes"></i></span>
                            <div class="media-body">
                                <h4>StrattonTrade Blog</h4>
                                <p>News and updates from the world’s leading cryptocurrency exchange.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="get-touch-content">
                        <div class="media">
                            <span><i class="fa fa-certificate"></i></span>
                            <div class="media-body">
                                <h4>Careers</h4>
                                <p>Help build the future of technology. Start your new career at StrattonTrade.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="get-touch-content">
                        <div class="media">
                            <span><i class="fa fa-life-ring"></i></span>
                            <div class="media-body">
                                <h4>Community</h4>
                                <p>StrattonTrade is global. Join the discussion in our worldwide communities.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-12 col-md-12">
                    <div class="col-xl-12 col-md-12">
                        <div
                            style="height:62px; background-color: #FFFFFF; overflow:hidden; box-sizing: border-box; border: 1px solid #56667F; border-radius: 4px; text-align: right; line-height:14px; block-size:62px; font-size: 12px; font-feature-settings: normal; text-size-adjust: 100%; box-shadow: inset 0 -20px 0 0 #56667F;padding:1px;padding: 0px; margin: 0px; width: 100%;">
                            <div style="height:40px; padding:0px; margin:0px; width: 100%;">
                                <iframe
                                    src="https://widget.coinlib.io/widget?type=horizontal_v2&theme=light&pref_coin_id=1505&invert_hover="
                                    width="100%" height="36px" scrolling="auto" marginwidth="0" marginheight="0"
                                    frameborder="0" border="0" style="border:0;margin:0;padding:0;"></iframe>
                            </div>
                            <div
                                style="color: #FFFFFF; line-height: 14px; font-weight: 400; font-size: 11px; box-sizing: border-box; padding: 2px 6px; width: 100%; font-family: Verdana, Tahoma, Arial, sans-serif;">
                                <a href="https://coinlib.io" target="_blank"
                                   style="font-weight: 500; color: #FFFFFF; text-decoration:none; font-size:11px">Cryptocurrency
                                    Prices</a>&nbsp;by Coinlib
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bottom section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                    <div class="bottom-logo">
                        {{--                        <img class="pb-3" src="images/logo-white.png" alt="">--}}

                        <h2 class="text-white">StrattonTrade LTD</h2>
                        <p> is a commodities broker that provides trading solutions sophisticated enough for veteran traders, yet simple enough for the forex novice.</p>

                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6">
                    <div class="bottom-widget">
                        <h4 class="widget-title">Company</h4>
                        <ul>
                            <li><a href="{{ url('/about') }}">About</a></li>
                            <li><a href="{{ url('/contact') }}">Contact</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6">
                    <div class="bottom-widget">
                        <h4 class="widget-title">Support</h4>
                        <ul>
                            <li><a href="#">Ticket</a></li>
                            <li><a href="#">FAQ</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Helpdesk</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6">
                    <div class="bottom-widget">
                        <h4 class="widget-title">Address</h4>
                        <p>1979 Marcus Avenue, Lake Success, New York, US</p>
{{--                        <ul>--}}
{{--                            <li><a href="#">Ticket</a></li>--}}
{{--                            <li><a href="#">FAQ</a></li>--}}
{{--                            <li><a href="#">Blog</a></li>--}}
{{--                            <li><a href="#">Helpdesk</a></li>--}}
{{--                        </ul>--}}
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6">
                    <div class="bottom-widget">
                        <h4 class="widget-title">Contact</h4>
                        <p> +13479190115</p>
                        <p> info@strattontrade.com</p>
{{--                        <ul>--}}
{{--                            <li><a href="#">Ticket</a></li>--}}
{{--                            <li><a href="#">FAQ</a></li>--}}
{{--                            <li><a href="#">Blog</a></li>--}}
{{--                            <li><a href="#">Helpdesk</a></li>--}}
{{--                        </ul>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

