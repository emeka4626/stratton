@extends('scaffold.main')

@section('body')
    <div class="content-body">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-xl-9 col-md-8">
                    <x-create-profile :user="$user"></x-create-profile>
                </div>
            </div>
        </div>
    </div>
@endsection

