@extends('scaffold.main')

@section('top-section')
    @include('scaffold.includes')
@endsection

@section('body')

    <div class="content-body">
        <div class="verification">
            <div class="container">
                <div class="row justify-content-center h-100 align-items-center  my-5">
                    <div class="col-xl-5 col-md-6">
                        <div class="auth-form card">
                            <div class="card-header">
                                <h4 class="card-title">Link a bank account</h4>
                            </div>
                            <div class="card-body">
                                <form action="{{ route('verify.save-bank') }}" method="POST" class="identity-upload">
                                    @csrf
                                    <div class="form-row">
                                        <div class="form-group col-xl-12">
                                            <label class="mr-sm-2">Bank Name </label>
                                            <input type="text" class="form-control @error('name') 'is-invalid' @enderror" placeholder="City Bank" name="name">
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-xl-12">
                                            <label class="mr-sm-2">Routing number </label>
                                            <input type="text" class="form-control @error('routing') 'is-invalid' @enderror" placeholder="25487" name="routing">
                                            @error('routing')
                                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ __("routing number is required") }}</strong>
                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-xl-12">
                                            <label class="mr-sm-2">Account number </label>
                                            <input type="text" class="form-control @error('number') 'is-invalid' @enderror" placeholder="36475" name="number">
                                            @error('phone')
                                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ __("Account number is required") }}</strong>
                                </span>
                                            @enderror
                                        </div>
                                        {{--                                    <div class="form-group col-xl-12">--}}
                                        {{--                                        <img src="{{ asset('images/routing.png') }}" alt="" class="img-fluid">--}}
                                        {{--                                    </div>--}}

                                        <div class="text-center col-12">
                                            <a href="{{ route('account.settings-account', auth()->user()) }}" class="btn btn-primary mx-2">Back</a>
                                            <button type="submit" class="btn btn-success mx-2">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-scripts')

@endsection
