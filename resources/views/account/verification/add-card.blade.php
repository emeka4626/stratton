@extends('scaffold.main')

@section('top-section')
    @include('scaffold.includes')
@endsection

@section('body')
    <div class="content-body">
        <div class="verification">
            <div class="container h-100">
                <div class="row justify-content-center h-100 align-items-center">
                    <div class="col-xl-5 col-md-6">
                        <div class="auth-form card">
                            <div class="card-header">
                                <h4 class="card-title">Link a debit card</h4>
                            </div>
                            <div class="card-body">
                                <form action="{{ route('verify.save-card') }}" method="post" class="identity-upload">
                                    @csrf
                                    <div class="form-row">
                                        <div class="form-group col-xl-12">
                                            <label class="mr-sm-2">Name on card </label>
                                            <input type="text" class="form-control" placeholder="Name" value="{{ auth()->user()->name() }}">
                                        </div>
                                        <div class="form-group col-xl-12">
                                            <label class="mr-sm-2">Card number </label>
                                            <input type="text" name="number" class="form-control @error('card') 'is-invalid' @enderror" placeholder="5658 4258 6358 4756" value="{{ old('number') }}">
                                            @error('number')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ __("Card number is required") }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-xl-4">
                                            <label class="mr-sm-2">Expiration </label>
                                            <input type="text" name="expiration" class="form-control @error('expiration') 'is-invalid' @enderror" placeholder="10/22" value="{{ old('expiration') }}">
                                            @error('expiration')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ __("Card expiration date is required") }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-xl-4">
                                            <label class="mr-sm-2">CVC </label>
                                            <input type="text" class="form-control @error('cvc') 'is-invalid' @enderror" placeholder="125" name="cvc" value="{{ old('cvc') }}">
                                            @error('cvc')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ __("CVC code is required") }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-xl-4">
                                            <label class="mr-sm-2">Postal code </label>
                                            <input type="text" class="form-control @error('postal') 'is-invalid' @enderror" placeholder="2368" name="postal" value="{{ old('postal') }}">
                                            @error('postal')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ __("Postal code is required") }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="text-center col-12">
                                            <button type="submit" class="btn btn-success pl-5 pr-5">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-scripts')

@endsection
