@extends('scaffold.main')

@section('top-section')
    @include('scaffold.includes')
@endsection

@section('body')
    <div class="content-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-header border-0">
                            <h4 class="card-title">Investment History</h4>
                        </div>
                        <div class="card-body pt-0">
                            <div class="transaction-table">
                                <div class="table-responsive">
                                    <table class="table mb-0 table-responsive-sm">
                                        <tr>
                                            <th>id</th>
                                            <th>status</th>
                                            <th>Amount Invested</th>
                                            <th>Plan</th>
                                            <th>Percentage</th>
                                            <th>Earning</th>
                                        </tr>
                                        <tbody>
                                        @foreach($investments as $investment)
                                            <tr>
                                                <td><span class="buy-thumb @if($investment->status == 'PENDING') bg-warning @else bg-success @endif"><i class="la @if($investment->status == 'PENDING') la-arrow-right @else la-arrow-up @endif"></i></span>
                                                </td>

                                                <td>
                                                    <span class="badge @if($investment->status == 'PENDING') badge-warning @else badge-success @endif">{{ $investment->status }}</span>
                                                </td>
                                                <td>+{{ number_format($investment->amount) }} USD</td>
                                                <td>{{ $investment->plan->name }}</td>
                                                <td>{{ $investment->plan->percentage }}</td>
                                                <td>${{ number_format($investment->earning()) }}</td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('extra-scripts')

@endsection
