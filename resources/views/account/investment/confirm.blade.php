@extends('scaffold.main')

@section('top-section')
    @include('scaffold.includes')
@endsection

@section('body')

    <div class="content-body">
        <div class="verification">
            <div class="container">
                <div class="row justify-content-center h-100 align-items-center  my-5">
                    <div class="col-xl-8 col-md-6">
                        <div class="auth-form card">
                            <div class="card-header">
                                <h4 class="card-title">Complete Transaction</h4>
                            </div>
                            <div class="card-body">
                                <div class="col-md-12 p-8">
                                    <div class="row justify-content-center p-8">
                                        <div class="col-xl-9 col-md-8">
                                            <p class="text-center">Pay ( {{ session()->get('btc') }} BTC ) to the given wallet address</p>
                                            <div class="row justify-content-center text-center">
                                                <q-component :value="'1FrVPiGSKQSfFvcm2zQkMRNhBT9aRzDAHL'" />
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon3"><i class="la la-itunes-note"></i></span>
                                                </div>
                                                <input type="text" value="1FHAXVP1CCL67bbHJdoUmVWmV2TG9YzmjK" class="form-control">
                                            </div>
                                            <div class="text-center mt-3">
                                                <a href="{{ route('invest.index', auth()->user()) }}" class="btn btn-success">Fund Again</a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-scripts')

@endsection

