@extends('scaffold.main')

@section('top-section')
    @include('scaffold.includes')
@endsection

@section('body')

    <div class="content-body">
        <div class="verification">
            <div class="container">
                <div class="row justify-content-center h-100 align-items-center  my-5">
                    <div class="col-xl-5 col-md-6">
                        <div class="auth-form card">
                            <div class="card-header">
                                <h4 class="card-title">Fund Wallet</h4>
                            </div>
                            <div class="card-body">
                                <form action="{{ route('account.fundWallet', auth()->user()) }}" method="POST" class="identity-upload">
                                    @csrf
                                    <div class="form-row">
                                        <div class="form-group col-xl-12">
                                            <label class="mr-sm-2" for="amount">Amount </label>
                                            <input type="text" id="amount" class="form-control @error('amount') 'is-invalid' @enderror" placeholder="$500" name="amount">
                                            @error('amount')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="text-center col-12">
                                            <a href="{{ route('account.dashboard', auth()->user()) }}" class="btn btn-primary mx-2">Back</a>
                                            <button type="submit" class="btn btn-success mx-2">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-scripts')

@endsection
