@extends('scaffold.main')

@section('top-section')
    @include('scaffold.includes')
@endsection

@section('body')
    <div class="content-body">
        <div class="container-fluid">
            <div class="row">
                <buy-sell-component></buy-sell-component>
                <transactions-component :class-name="'col-xl-7 col-lg-7 col-md-12'" :transactions="{{ $transactions }}"></transactions-component>
            </div>
        </div>
    </div>
@endsection

@section('extra-scripts')
    <script>
        import TransactionsComponent from "../../js/components/trading/TransactionsComponent";
        import BuySellComponent from "../../js/components/trading/BuySellComponent";
        export default {
            components: {BuySellComponent, TransactionsComponent}
        }
    </script>
@endsection
