@extends('scaffold.main')

@section('top-section')
    @include('scaffold.includes')
@endsection

@section('body')
    <div class="content-body">
        <div class="container-fluid">
            <div class="row">
                <x-setting-sidebar></x-setting-sidebar>
                <div class="col-xl-9 col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Linked Account or Card</h4>
                        </div>
                        <div class="card-body">
                            <div class="form">
                                <ul class="linked_account">
                                    @foreach($user->banks as $bank)
                                        <li>
                                            <div class="row">
                                                <div class="col-9">
                                                    <div class="media">
                                                        <span class="mr-3"><i class="fa fa-bank"></i></span>
                                                        <div class="media-body">
                                                            <h5 class="mt-0 mb-1 capitalize">{{ $bank->name }}</h5>
                                                            <p>Bank **************5421</p>
                                                        </div>
                                                        <div class="edit-option">
                                                            <a href="#"><i class="fa fa-eye"></i></a>
                                                            <a href="#"><i class="fa fa-trash"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-3">
                                                    <div class="verify">
                                                        <div class="verified">
                                                            <span class="bg-{{ $bank->status ? 'success' : 'danger' }}">
                                                                <i  class="la la-{{ $bank->status ? 'check' : 'close' }}"></i>
                                                            </span>
                                                            <a href="#">{{ $bank->status ? 'Verified' : 'Pending' }}</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach

                                    @foreach($user->cards as $card)
                                        <li>
                                            <div class="row">
                                                <div class="col-9">
                                                    <div class="media my-4">
                                                        <span class="mr-3"><i class="fa fa-cc-mastercard"></i></span>
                                                        <div class="media-body">
                                                            <h5 class="mt-0 mb-1">Master Card</h5>
                                                            <p>Credit Card *********5478 </p>
                                                        </div>
                                                        <div class="edit-option">
                                                            <a href="#"><i class="fa fa-eye"></i></a>
                                                            <a href="#"><i class="fa fa-pencil"></i></a>
                                                            <a href="#"><i class="fa fa-trash"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-3">
                                                    <div class="verify">
                                                        <div class="verified">
                                                            <span class="bg-{{ $card->status ? 'success' : 'danger' }}">
                                                                <i  class="la la-{{ $card->status ? 'check' : 'close' }}"></i>
                                                            </span>
                                                            <a href="#">{{ $card->status ? 'Verified' : 'Pending' }}</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>

                                <div class="mt-5">
                                    <a href="{{ route('verify.add-bank') }}" class="btn btn-primary px-4 mr-3">Add Bank
                                        Account</a>
                                    <a href="{{ route('verify.add-card') }}" class="btn btn-success px-4">Add Debit
                                        Account</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-scripts')

@endsection
