@extends('scaffold.main')

@section('top-section')
    @include('scaffold.includes')
@endsection

@section('body')
    <div class="content-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-header border-0">
                            <h4 class="card-title">Transaction History</h4>
                        </div>
                        <div class="card-body pt-0">
                            <div class="transaction-table">
                                <div class="table-responsive">
                                    <table class="table mb-0 table-responsive-sm">
                                        <tbody>
                                        @foreach($transactions as $transaction)
                                            <tr>
                                                <td><span class="buy-thumb"><i class="la la-arrow-up"></i></span>
                                                </td>

                                                <td>
                                                    <span class="badge badge-success">Buy</span>
                                                </td>
                                                <td>
                                                    <i class="cc BTC"></i> BTC
                                                </td>
                                                <td class="text-success">+{{ $transaction->value }} BTC</td>
                                                <td>+{{ $transaction->amount }} USD</td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('extra-scripts')

@endsection
