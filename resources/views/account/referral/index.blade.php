@extends('scaffold.main')

@section('top-section')
    @include('scaffold.includes')
@endsection

@section('body')
    <div class="content-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-5">
                    <div class="card">
                        <div class="card-header border-0">
                            <p class="alert alert-info">Your Referral Link: {{ route('register', ['ref' => auth()->user()->referral_code]) }}</p>
                        </div>
                        <div class="card-body pt-0">
                            <p>Refer a friend and get instant 10% on their deposit</p>
                            <br>
                            <form method="post" action="{{ route('refer') }}">
                                @csrf
                                <label for="basic-url">Or Send a referral mail to your friend</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon3"><i class="la la-edit"></i></span>
                                    </div>
                                    <input type="email" name="email" value="" class="form-control" placeholder="Email Addresss">


                                </div>

                                <button type="submit" class="btn btn-block btn-success">Send Invitation</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-xl-7">
                    <div class="card">
                        <div class="card-header border-0">
                            <h4 class="card-title">Referral History</h4>
                            <span>Earned: $0</span>
                        </div>
                        <div class="card-body pt-0">
                            <div class="transaction-table">
                                <div class="table-responsive">
                                    <table class="table mb-0 table-responsive-sm">
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Trade Count</th>
                                            <th>Earned</th>
                                        </tr>
                                        <tbody>
                                        @foreach($referrals as $key => $referral)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ $referral->referral->name() }}</td>
                                                <td>{{ $referral->referral->investments->count() }}</td>
                                                <td>0</td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('extra-scripts')

@endsection
