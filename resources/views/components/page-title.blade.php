<div class="page-title dashboard">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12">
                <div class="page-title-content">
                    <p>Referral Code:
                        <span> {{ $referral_code }}</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
