<div class="header dashboard">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12">
                <nav class="px-0 navbar navbar-expand-lg navbar-light justify-content-between">
                    @auth
                        <a class="navbar-brand" href="{{ route('account.dashboard', auth()->user()) }}">
                            {{--                        <img src="{{ asset('images/logo.png') }}" alt="">--}}
                            <h4>OxenBid</h4>
                        </a>
                    @else
                        <a class="navbar-brand" href="{{ url('/') }}">
                            {{--                        <img src="{{ asset('images/logo.png') }}" alt="">--}}
                            <h4>OxenBid</h4>
                        </a>
                    @endauth


                    <div class="my-2 dashboard_log">
                        <div class="d-flex align-items-center">
                            <div class="account_money">
                                <ul>
                                    <li class="crypto">
                                        <span>{{ $btcBalance }}</span>
                                        <i class="cc BTC-alt"></i>
                                    </li>
                                    <li class="usd">
                                        <span>{{ $usdBalance }} USD</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="profile_log dropdown">
                                <div class="user" data-toggle="dropdown">
                                    <span class="thumb"><i class="la la-user"></i></span>
                                    <span class="name">{{ $user->firstName() }}</span>
                                    <span class="arrow"><i class="la la-angle-down"></i></span>
                                </div>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="{{ route('account.accounts', $user) }}" class="dropdown-item">
                                        <i class="la la-user"></i> Account
                                    </a>
                                    <a href="{{ route('account.transactions', $user) }}" class="dropdown-item">
                                        <i class="la la-book"></i> History
                                    </a>
                                    <a href="{{ route('account.settings', $user) }}" class="dropdown-item">
                                        <i class="la la-cog"></i> Setting
                                    </a>
                                    <a href="#" onclick="document.querySelector('#logout-form').submit()" class="dropdown-item logout">
                                        <i class="la la-sign-out"></i> Logout
                                    </a>
                                </div>
                                <form action="{{ route('logout') }}" method="post" id="logout-form">
                                    @csrf
                                </form>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>
