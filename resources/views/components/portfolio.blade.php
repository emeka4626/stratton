
    <div class="col-xl-12 col-lg-12 col-xxl-12">
        <div class="card balance-widget">
            <div class="card-header border-0 py-0">
                <h4 class="card-title">Your Portfolio </h4>
            </div>
            <div class="card-body pt-0">
                <div class="balance-widget">
                    <div class="total-balance">
                        <h3>${{ number_format($usdBalance) }}</h3>
                        <h6>Total Balance</h6>
                    </div>
                    <ul class="list-unstyled">
                        <li class="media">
                            <i class="cc BTC mr-3"></i>
                            <div class="media-body">
                                <h5 class="m-0">Bitcoin</h5>
                            </div>
                            <div class="text-right">
                                <h5>{{ $btcBalance }} BTC</h5>
                                <span>{{ $usdBalance }} USD</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

