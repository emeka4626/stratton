<div class="col-xl-3 col-md-4">
    <div class="card settings_menu">
        <div class="card-header">
            <h4 class="card-title">Settings</h4>
        </div>
        <div class="card-body">
            <ul>
                <li class="nav-item">
                    <a href="{{ route('account.settings', auth()->user()) }}" class="nav-link active">
                        <i class="la la-user"></i>
                        <span>Edit Profile</span>
                    </a>
                </li>
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="./settings-security.html" class="nav-link">--}}
                {{--                                        <i class="la la-lock"></i>--}}
                {{--                                        <span>Security</span>--}}
                {{--                                    </a>--}}
                {{--                                </li>--}}
                <li class="nav-item">
                    <a href="{{ route('account.settings-account', auth()->user()) }}" class="nav-link">
                        <i class="la la-university"></i>
                        <span>Linked Account</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
