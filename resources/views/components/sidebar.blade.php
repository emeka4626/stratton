<div class="sidebar">
    <div class="menu">
        <ul>
            <li>
                <a href="{{ route('account.dashboard', $user) }}" data-toggle="tooltip" data-placement="right" title="Home">
                    <span><i class="la la-igloo"></i></span>
                </a>
            </li>
            <li>
                <a href="{{ route('invest.index', $user) }}" data-toggle="tooltip" data-placement="right" title="Fund Wallet">
                    <span><i class="la la-wallet"></i></span>
                </a>
            </li>
            <li><a href="{{ route('account.buy', $user) }}" data-toggle="tooltip" data-placement="right" title="Exchange">
                    <span><i class="la la-exchange-alt"></i></span>
                </a>
            </li>
            <li><a href="{{ route('investment', $user) }}" data-toggle="tooltip" data-placement="right" title="investments">
                    <span><i class="la la-industry"></i></span>
                </a>
            </li>
            <li><a href="{{ route('account.accounts', $user) }}" data-toggle="tooltip" data-placement="right" title="Account">
                    <span><i class="la la-user"></i></span>
                </a>
            </li>
            <li><a href="{{ route('account.settings', $user) }}" data-toggle="tooltip" data-placement="right" title="Setting">
                    <span><i class="la la-tools"></i></span>
                </a>
            </li>

            <li><a href="{{ route('referral.index') }}" data-toggle="tooltip" data-placement="right" title="Referrals">
                    <span><i class="la la-share"></i></span>
                </a>
            </li>
        </ul>
    </div>
</div>
