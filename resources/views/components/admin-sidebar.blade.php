<div class="sidebar">
    <div class="menu">
        <ul>
            <li>
                <a href="{{ route('admin.dashboard', $user) }}" data-toggle="tooltip" data-placement="right" title="Home">
                    <span><i class="la la-house-damage"></i></span>
                </a>
            </li>
            <li><a href="{{ route('admin.btcTransaction', $user) }}" data-toggle="tooltip" data-placement="right" title="Exchange">
                    <span><i class="la la-btc"></i></span>
                </a>
            </li>
            <li><a href="{{ route('admin.investments', $user) }}" data-toggle="tooltip" data-placement="right" title="Investments">
                    <span><i class="la la-exchange-alt"></i></span>
                </a>
            </li>
            <li><a href="{{ route('admin.withdrawals', $user) }}" data-toggle="tooltip" data-placement="right" title="Withdrawals">
                    <span><i class="la la-wallet"></i></span>
                </a>
            </li>
            <li><a href="{{ route('admin.users', $user) }}" data-toggle="tooltip" data-placement="right" title="Users">
                    <span><i class="la la-users"></i></span>
                </a>
            </li>
            <li><a href="{{ route('admin.settings', $user) }}" data-toggle="tooltip" data-placement="right" title="settings">
                    <span><i class="la la-gear"></i></span>
                </a>
            </li>
        </ul>
    </div>
</div>
