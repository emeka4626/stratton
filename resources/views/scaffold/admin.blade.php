@extends('layouts.app')
@section('loader')
    @include('partials.loader')
@endsection

@section('content')
    <div id="main-wrapper">
        @yield('top-section')

        @yield('body')
        <x-footer></x-footer>
    </div>

@endsection

@section('scripts')
    @yield('extra-scripts')
@endsection
