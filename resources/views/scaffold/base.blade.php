@extends('layouts.app')
@section('loader')
    @include('partials.loader')
@endsection

@section('content')
    @yield('body')
@endsection
