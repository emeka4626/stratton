@extends('scaffold.base')


@section('body')
    @include('partials.header')
    <div class="about-one section-padding">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="service-img">
                        <img src="{{ asset('images/about/1.jpg') }}" alt="" class="img-fluid">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="service-content m-t-50">
                        <h3>In Brief</h3>
                        <p>
                            Oxenbid is one of the most trusted and fastest growing crypto exchanges and Investment platform in existence. With an unparalleled track record of reliability even while operating offline, we offer our clients a comprehensive range of services that are accented by our smooth order execution, state-of-the-art trading terminal, and unmatched liquidity.
                        </p>
                        <p>
                            Oxenbid is an online commodity trading platform providing traders across the globe with cutting edge technology to trade the world’s markets
                            Our exchange is the largest spot trading market in the industry with over 900 trading pairs and 500+ spot instruments supported, including Bitcoin, BitcoinCash, EOS, Litecoin, Tron and others.
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="about-one section-padding">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="service-img">
                        <img src="{{ asset('images/background/2.jpg') }}" alt="" class="img-fluid">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="service-content m-t-50">
                        <h3>Missions</h3>
                        <p>
                            Oxenbid is all about ease of use. Our mission is to make Bitcoin exchange and investment in digital assets as easy as ordering a taxi. We offer you the simplest and fastest access to buying or selling and investing in Bitcoin.
                        </p>
                        <p>
                            We show you where you are, protect you from financial dangers, create a roadmap for your future, manage your investments, and provide advice that touches every area of your life.
                        </p>
                        <p>
                            An easy verification process gives you access to an intuitive, fast and secure experience. To stay ahead of the curve, we continuously improve our platform while expanding our ideas to introduce new services.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="about-one section-padding">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="service-img">
                        <img src="{{ asset('images/background/bg.jpg') }}" alt="" class="img-fluid">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="service-content m-t-50">
                        <h3>Our Core Values</h3>
                        <p>
                            Our values are based on three tenets: reliability, security, and performance. With our deep understanding of technology, economics, and finance, and by applying knowledge and innovation, we have created one of the most sophisticated and technologically advanced trading platforms in the industry. Since its introduction, our comprehensive custody architecture has been an industry standard-bearer in security.
                        </p>
                        <p>
                            Our goal is to identify unique investment opportunities and work with the portfolio managers and/or companies to source the capital needed to grow the business.
                        </p>

                        <p>
                            We use two different types of custody: top-tier security for the top 10 cryptocurrency on the market, including all ETH tokens, and regular security for all other digital assets.
                            Top-tier security custody is based on HDKey technology and multilevel in-house cryptography. There is a strict policy of not running third-party web App/ software, including third-party blockchain solutions.
                            We ensure that our traders get the best pricing on the market. We are able to do this by providing incentives for market makers and using the best and latest technology (ultra-low latency matching in LD4, United State; smooth API, and an advanced reporting system).
                            We believe in a future with digital economies and a self-sustainable internet at its core; and for many years, we have been actively creating the vital infrastructure needed for this future. We have expended substantial effort to ensure that we comply with the ever-changing and evolving regulations in the digital asset space.
                            As part of this, we implemented practices necessary to insulate ourselves from bad actors and establish fast and secure operations (KYC/KYT/Market Surveillance/AML) in full compliance with the regulatory framework.
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
