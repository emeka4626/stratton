<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Welcome - Oxenbid</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="img/logo/favicon.ico">

    <!-- all css here -->

    <!-- bootstrap v3.3.6 css -->
    <link rel="stylesheet" href="{{ asset('v2/css/bootstrap.min.css') }}">
    <!-- owl.carousel css -->
    <link rel="stylesheet" href="{{ asset('v2/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('v2/css/owl.transitions.css') }}">
    <!-- Animate css -->
    <link rel="stylesheet" href="{{ asset('v2/css/animate.css') }}">
    <!-- meanmenu css -->
    <link rel="stylesheet" href="{{ asset('v2/css/meanmenu.min.css') }}">
    <!-- font-awesome css -->
    <link rel="stylesheet" href="{{ asset('v2/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('v2/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('v2/css/flaticon.css') }}">
    <!-- magnific css -->
    <link rel="stylesheet" href="{{ asset('v2/css/magnific.min.css') }}">
    <!-- style css -->
    <link rel="stylesheet" href="{{ asset('v2/style3.css') }}">
    <!-- responsive css -->
    <link rel="stylesheet" href="{{ asset('v2/css/responsive.css') }}">

    <!-- modernizr css -->
    <script src="{{ asset('v2/js/vendor/modernizr-2.8.3.min.js') }}"></script>
</head>

<body>

    <!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

    {{--<div id="preloader"></div>--}}
    <header class="header-one">
        <!-- Start top bar -->
        <div class="topbar-area fix hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="topbar-left">
                            <ul>
                                <li><a href="#"><i class="fa fa-envelope"></i> info@Oxenbid.com</a></li>
                                <li><a href="#"><i class="fa fa-phone"></i> +18324797080</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End top bar -->
        <!-- header-area start -->
        <div id="sticker" class="header-area header-area-2 hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="row">
                            <!-- logo start -->
                            <div class="col-md-3 col-sm-3">
                                <div class="logo">
                                    <!-- Brand -->
                                    <a class="navbar-brand page-scroll white-logo" href="{{ url('/') }}">
                                        {{-- <img src="{{ asset('v2/img/logo/logo3.png') }}" alt="">--}}
                                        <h3 class="text-white" style="color: #FFFFFF;">Oxenbid</h3>
                                    </a>
                                    <a class="navbar-brand page-scroll black-logo" href="{{ url('/') }}">
                                        <h3 class="text-white">Oxenbid</h3>
                                    </a>
                                </div>
                                <!-- logo end -->
                            </div>
                            <div class="col-md-9 col-sm-9">
                                <div class="header-right-link">
                                    <!-- search option end -->
                                    @auth
                                    <a href="{{ route('account.dashboard', auth()->user()) }}" class="s-menu">Dashboard</a>

                                    @else
                                    <div class="row">
                                        <a href="{{ url('login') }}" class="s-menu">Login</a>

                                    </div>
                                    @endauth
                                </div>
                                <!-- mainmenu start -->
                                <nav class="navbar navbar-default">
                                    <div class="collapse navbar-collapse" id="navbar-example">
                                        <div class="main-menu">
                                            <ul class="nav navbar-nav navbar-right">
                                                <li><a class="pages" href="{{ url('/') }}">Home</a>
                                                </li>
                                                <li><a href="{{ url('about') }}">Company</a></li>

                                                <li><a href="{{ url('contact') }}">Support</a></li>
                                                @guest
                                                <li><a href="{{ url('Register') }}">Register</a></li>
                                                @endguest
                                            </ul>
                                        </div>
                                    </div>
                                </nav>
                                <!-- mainmenu end -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- header-area end -->
        <!-- mobile-menu-area start -->
        <div class="mobile-menu-area hidden-lg hidden-md hidden-sm">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="mobile-menu">
                            <div class="logo">
                                <a href="{{ url('/') }}">Oxenbid</a>
                            </div>
                            <nav id="dropdown">
                                <ul>
                                    <li><a class="pages" href="{{ url('/') }}">Home</a>
                                    </li>
                                    @auth
                                    <li><a href="{{ route('account.dashboard', auth()->user()) }}">Login</a></li>
                                    @else
                                    <li><a href="{{ url('login') }}">Login</a></li>
                                    <li><a href="{{ url('register') }}">Register</a></li>
                                    @endauth
                                    <li><a href="{{ url('about') }}">Company</a></li>

                                    <li><a href="{{ url('contact') }}">Support</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- mobile-menu-area end -->
    </header>
    <!-- header end -->
    <!-- Start Slider Area -->
    <div class="intro-area">
        <div class="main-overly"></div>
        <div class="intro-carousel">
            <div class="intro-content">
                <div class="slider-images">
                    <img src="{{ asset('v2/img/slider/h1.jpg') }}" alt="">
                </div>
                <div class="slider-content">
                    <div class="display-table">
                        <div class="display-table-cell">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <!-- Start Slider content -->
                                        <div class="text-center slide-content">
                                            <h2 class="title2">{{ __("The original global crypto exchange") }}</h2>
                                            <!-- <h4 class="title3" style="color: #FFFFFF;">
                                        With a proven track record and a mature approach to the industry, we provide reliable trading of cryptocurrencies.
                                        </h4> -->
                                            <div class="layer-1-3">
                                                <a href="{{ route('register') }}" class="ready-btn left-btn">{{ __("Get started") }}</a>
                                            </div>
                                        </div>
                                        <!-- End Slider content -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="intro-content">
                <div class="slider-images">
                    <img src="{{ asset('v2/img/slider/h2.jpg') }}" alt="">
                </div>
                <div class="slider-content">
                    <div class="display-table">
                        <div class="display-table-cell">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <!-- Start Slider content -->
                                        <div class="text-center slide-content">
                                            <h2 class="title2">{{ __("Invest From Your Comfort Zone") }}</h2>
                                            <div class="layer-1-3">
                                                <a href="{{ route('register') }}" class="ready-btn left-btn">{{ __("Get started") }}</a>
                                            </div>
                                        </div>
                                        <!-- End Slider content -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Slider Area -->
    <!-- Start Count area -->
    <div class="counter-area fix area-padding-2">
        <div class="float">
            <!-- Start counter Area -->
            <div class="px-4" style="padding-left: 2.5rem; overflow: hidden">
                <div class="chart-carousel">
                    <div style="width: 400px; height:220px; background-color: #FFFFFF; overflow:hidden; box-sizing: border-box; border: 1px solid #56667F; border-radius: 4px; text-align: right; line-height:14px; block-size:220px; font-size: 12px; font-feature-settings: normal; text-size-adjust: 100%; box-shadow: inset 0 -20px 0 0 #56667F;padding:1px;padding: 0px; margin: 0px;">
                        <div style="height:200px; padding:0px; margin:0px; width: 100%;"><iframe src="https://widget.coinlib.io/widget?type=single_v2&theme=light&coin_id=859&pref_coin_id=1505" width="400" height="196px" scrolling="auto" marginwidth="0" marginheight="0" frameborder="0" border="0" style="border:0;margin:0;padding:0;line-height:14px;"></iframe></div>
                        <div style="color: #FFFFFF; line-height: 14px; font-weight: 400; font-size: 11px; box-sizing: border-box; padding: 2px 6px; width: 100%; font-family: Verdana, Tahoma, Arial, sans-serif;"><a href="https://coinlib.io" target="_blank" style="font-weight: 500; color: #FFFFFF; text-decoration:none; font-size:11px">Cryptocurrency Prices</a>&nbsp;by Coinlib</div>
                    </div>
                    <div style="width: 400px; height:220px; background-color: #FFFFFF; overflow:hidden; box-sizing: border-box; border: 1px solid #56667F; border-radius: 4px; text-align: right; line-height:14px; block-size:220px; font-size: 12px; font-feature-settings: normal; text-size-adjust: 100%; box-shadow: inset 0 -20px 0 0 #56667F;padding:1px;padding: 0px; margin: 0px;">
                        <div style="height:200px; padding:0px; margin:0px; width: 100%;"><iframe src="https://widget.coinlib.io/widget?type=single_v2&theme=light&coin_id=145&pref_coin_id=1505" width="400" height="196px" scrolling="auto" marginwidth="0" marginheight="0" frameborder="0" border="0" style="border:0;margin:0;padding:0;line-height:14px;"></iframe></div>
                        <div style="color: #FFFFFF; line-height: 14px; font-weight: 400; font-size: 11px; box-sizing: border-box; padding: 2px 6px; width: 100%; font-family: Verdana, Tahoma, Arial, sans-serif;"><a href="https://coinlib.io" target="_blank" style="font-weight: 500; color: #FFFFFF; text-decoration:none; font-size:11px">Cryptocurrency Prices</a>&nbsp;by Coinlib</div>
                    </div>
                    <div style="width: 400px; height:220px; background-color: #FFFFFF; overflow:hidden; box-sizing: border-box; border: 1px solid #56667F; border-radius: 4px; text-align: right; line-height:14px; block-size:220px; font-size: 12px; font-feature-settings: normal; text-size-adjust: 100%; box-shadow: inset 0 -20px 0 0 #56667F;padding:1px;padding: 0px; margin: 0px;">
                        <div style="height:200px; padding:0px; margin:0px; width: 100%;"><iframe src="https://widget.coinlib.io/widget?type=single_v2&theme=light&coin_id=619&pref_coin_id=1505" width="400" height="196px" scrolling="auto" marginwidth="0" marginheight="0" frameborder="0" border="0" style="border:0;margin:0;padding:0;line-height:14px;"></iframe></div>
                        <div style="color: #FFFFFF; line-height: 14px; font-weight: 400; font-size: 11px; box-sizing: border-box; padding: 2px 6px; width: 100%; font-family: Verdana, Tahoma, Arial, sans-serif;"><a href="https://coinlib.io" target="_blank" style="font-weight: 500; color: #FFFFFF; text-decoration:none; font-size:11px">Cryptocurrency Prices</a>&nbsp;by Coinlib</div>
                    </div>
                    <div style="width: 400px; height:220px; background-color: #FFFFFF; overflow:hidden; box-sizing: border-box; border: 1px solid #56667F; border-radius: 4px; text-align: right; line-height:14px; block-size:220px; font-size: 12px; font-feature-settings: normal; text-size-adjust: 100%; box-shadow: inset 0 -20px 0 0 #56667F;padding:1px;padding: 0px; margin: 0px;">
                        <div style="height:200px; padding:0px; margin:0px; width: 100%;"><iframe src="https://widget.coinlib.io/widget?type=single_v2&theme=light&coin_id=637&pref_coin_id=1505" width="400" height="196px" scrolling="auto" marginwidth="0" marginheight="0" frameborder="0" border="0" style="border:0;margin:0;padding:0;line-height:14px;"></iframe></div>
                        <div style="color: #FFFFFF; line-height: 14px; font-weight: 400; font-size: 11px; box-sizing: border-box; padding: 2px 6px; width: 100%; font-family: Verdana, Tahoma, Arial, sans-serif;"><a href="https://coinlib.io" target="_blank" style="font-weight: 500; color: #FFFFFF; text-decoration:none; font-size:11px">Cryptocurrency Prices</a>&nbsp;by Coinlib</div>
                    </div>
                    <div style="width: 400px; height:220px; background-color: #FFFFFF; overflow:hidden; box-sizing: border-box; border: 1px solid #56667F; border-radius: 4px; text-align: right; line-height:14px; block-size:220px; font-size: 12px; font-feature-settings: normal; text-size-adjust: 100%; box-shadow: inset 0 -20px 0 0 #56667F;padding:1px;padding: 0px; margin: 0px;">
                        <div style="height:200px; padding:0px; margin:0px; width: 100%;"><iframe src="https://widget.coinlib.io/widget?type=single_v2&theme=light&coin_id=157&pref_coin_id=1505" width="400" height="196px" scrolling="auto" marginwidth="0" marginheight="0" frameborder="0" border="0" style="border:0;margin:0;padding:0;line-height:14px;"></iframe></div>
                        <div style="color: #FFFFFF; line-height: 14px; font-weight: 400; font-size: 11px; box-sizing: border-box; padding: 2px 6px; width: 100%; font-family: Verdana, Tahoma, Arial, sans-serif;"><a href="https://coinlib.io" target="_blank" style="font-weight: 500; color: #FFFFFF; text-decoration:none; font-size:11px">Cryptocurrency Prices</a>&nbsp;by Coinlib</div>
                    </div>
                    <div style="width: 400px; height:220px; background-color: #FFFFFF; overflow:hidden; box-sizing: border-box; border: 1px solid #56667F; border-radius: 4px; text-align: right; line-height:14px; block-size:220px; font-size: 12px; font-feature-settings: normal; text-size-adjust: 100%; box-shadow: inset 0 -20px 0 0 #56667F;padding:1px;padding: 0px; margin: 0px;">
                        <div style="height:200px; padding:0px; margin:0px; width: 100%;"><iframe src="https://widget.coinlib.io/widget?type=single_v2&theme=light&coin_id=122882&pref_coin_id=1505" width="400" height="196px" scrolling="auto" marginwidth="0" marginheight="0" frameborder="0" border="0" style="border:0;margin:0;padding:0;line-height:14px;"></iframe></div>
                        <div style="color: #FFFFFF; line-height: 14px; font-weight: 400; font-size: 11px; box-sizing: border-box; padding: 2px 6px; width: 100%; font-family: Verdana, Tahoma, Arial, sans-serif;"><a href="https://coinlib.io" target="_blank" style="font-weight: 500; color: #FFFFFF; text-decoration:none; font-size:11px">Cryptocurrency Prices</a>&nbsp;by Coinlib</div>
                    </div>
                    <div style="width: 400px; height:220px; background-color: #FFFFFF; overflow:hidden; box-sizing: border-box; border: 1px solid #56667F; border-radius: 4px; text-align: right; line-height:14px; block-size:220px; font-size: 12px; font-feature-settings: normal; text-size-adjust: 100%; box-shadow: inset 0 -20px 0 0 #56667F;padding:1px;padding: 0px; margin: 0px;">
                        <div style="height:200px; padding:0px; margin:0px; width: 100%;"><iframe src="https://widget.coinlib.io/widget?type=single_v2&theme=light&coin_id=359&pref_coin_id=1505" width="400" height="196px" scrolling="auto" marginwidth="0" marginheight="0" frameborder="0" border="0" style="border:0;margin:0;padding:0;line-height:14px;"></iframe></div>
                        <div style="color: #FFFFFF; line-height: 14px; font-weight: 400; font-size: 11px; box-sizing: border-box; padding: 2px 6px; width: 100%; font-family: Verdana, Tahoma, Arial, sans-serif;"><a href="https://coinlib.io" target="_blank" style="font-weight: 500; color: #FFFFFF; text-decoration:none; font-size:11px">Cryptocurrency Prices</a>&nbsp;by Coinlib</div>
                    </div>
                    <div style="width: 400px; height:220px; background-color: #FFFFFF; overflow:hidden; box-sizing: border-box; border: 1px solid #56667F; border-radius: 4px; text-align: right; line-height:14px; block-size:220px; font-size: 12px; font-feature-settings: normal; text-size-adjust: 100%; box-shadow: inset 0 -20px 0 0 #56667F;padding:1px;padding: 0px; margin: 0px;">
                        <div style="height:200px; padding:0px; margin:0px; width: 100%;"><iframe src="https://widget.coinlib.io/widget?type=single_v2&theme=light&coin_id=489&pref_coin_id=1505" width="400" height="196px" scrolling="auto" marginwidth="0" marginheight="0" frameborder="0" border="0" style="border:0;margin:0;padding:0;line-height:14px;"></iframe></div>
                        <div style="color: #FFFFFF; line-height: 14px; font-weight: 400; font-size: 11px; box-sizing: border-box; padding: 2px 6px; width: 100%; font-family: Verdana, Tahoma, Arial, sans-serif;"><a href="https://coinlib.io" target="_blank" style="font-weight: 500; color: #FFFFFF; text-decoration:none; font-size:11px">Cryptocurrency Prices</a>&nbsp;by Coinlib</div>
                    </div>
                    <div style="width: 400px; height:220px; background-color: #FFFFFF; overflow:hidden; box-sizing: border-box; border: 1px solid #56667F; border-radius: 4px; text-align: right; line-height:14px; block-size:220px; font-size: 12px; font-feature-settings: normal; text-size-adjust: 100%; box-shadow: inset 0 -20px 0 0 #56667F;padding:1px;padding: 0px; margin: 0px;">
                        <div style="height:200px; padding:0px; margin:0px; width: 100%;"><iframe src="https://widget.coinlib.io/widget?type=single_v2&theme=light&coin_id=8550&pref_coin_id=1505" width="400" height="196px" scrolling="auto" marginwidth="0" marginheight="0" frameborder="0" border="0" style="border:0;margin:0;padding:0;line-height:14px;"></iframe></div>
                        <div style="color: #FFFFFF; line-height: 14px; font-weight: 400; font-size: 11px; box-sizing: border-box; padding: 2px 6px; width: 100%; font-family: Verdana, Tahoma, Arial, sans-serif;"><a href="https://coinlib.io" target="_blank" style="font-weight: 500; color: #FFFFFF; text-decoration:none; font-size:11px">Cryptocurrency Prices</a>&nbsp;by Coinlib</div>
                    </div>
                    <div style="width: 400px; height:220px; background-color: #FFFFFF; overflow:hidden; box-sizing: border-box; border: 1px solid #56667F; border-radius: 4px; text-align: right; line-height:14px; block-size:220px; font-size: 12px; font-feature-settings: normal; text-size-adjust: 100%; box-shadow: inset 0 -20px 0 0 #56667F;padding:1px;padding: 0px; margin: 0px;">
                        <div style="height:200px; padding:0px; margin:0px; width: 100%;"><iframe src="https://widget.coinlib.io/widget?type=single_v2&theme=light&coin_id=1188&pref_coin_id=1505" width="400" height="196px" scrolling="auto" marginwidth="0" marginheight="0" frameborder="0" border="0" style="border:0;margin:0;padding:0;line-height:14px;"></iframe></div>
                        <div style="color: #FFFFFF; line-height: 14px; font-weight: 400; font-size: 11px; box-sizing: border-box; padding: 2px 6px; width: 100%; font-family: Verdana, Tahoma, Arial, sans-serif;"><a href="https://coinlib.io" target="_blank" style="font-weight: 500; color: #FFFFFF; text-decoration:none; font-size:11px">Cryptocurrency Prices</a>&nbsp;by Coinlib</div>
                    </div>
                    <div style="width: 400px; height:220px; background-color: #FFFFFF; overflow:hidden; box-sizing: border-box; border: 1px solid #56667F; border-radius: 4px; text-align: right; line-height:14px; block-size:220px; font-size: 12px; font-feature-settings: normal; text-size-adjust: 100%; box-shadow: inset 0 -20px 0 0 #56667F;padding:1px;padding: 0px; margin: 0px;">
                        <div style="height:200px; padding:0px; margin:0px; width: 100%;"><iframe src="https://widget.coinlib.io/widget?type=single_v2&theme=light&coin_id=648685&pref_coin_id=1505" width="400" height="196px" scrolling="auto" marginwidth="0" marginheight="0" frameborder="0" border="0" style="border:0;margin:0;padding:0;line-height:14px;"></iframe></div>
                        <div style="color: #FFFFFF; line-height: 14px; font-weight: 400; font-size: 11px; box-sizing: border-box; padding: 2px 6px; width: 100%; font-family: Verdana, Tahoma, Arial, sans-serif;"><a href="https://coinlib.io" target="_blank" style="font-weight: 500; color: #FFFFFF; text-decoration:none; font-size:11px">Cryptocurrency Prices</a>&nbsp;by Coinlib</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Count area -->
    <!-- Start Invest area -->
    <div class="invest-area bg-color">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="text-center section-headline">
                        <h3>Oxenbid Investment</h3>
                        <p>the fastest and secure way to invest or exchange 150+ cryptocurrencies</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="pricing-content">
                    @foreach($plans as $plan)
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="pri_table_list">
                            <div class="top-price-inner">
                                <span class="base">{{ $plan->name }}</span>
                                <div class="rates">
                                    <span class="prices">{{ $plan->percentage }}%</span>
                                </div>
                                <span class="per-day">{{ $plan->duration }} days</span>
                            </div>
                            <ol class="pricing-text">
                                <li class="check">Minimum Invest : ${{ number_format($plan->min) }}</li>
                                <li class="check">Maximum Invest : ${{ number_format($plan->max) }}</li>
                            </ol>
                            <div class="price-btn blue">
                                <a class="blue" href="{{ url('/home') }}">Deposit</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- End Invest area -->
    <!-- Start Support-service Area -->
    <div class="support-service-area fix area-padding-2">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="text-center section-headline">
                        <h3>Why choose Oxenbid</h3>
                        <p>Oxenbid has a variety of features that make it the best place to start trading</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="support-all">
                    <!-- Start About -->
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="support-services wow ">
                            <a class="support-images" href="#"><i class="flaticon-023-management"></i></a>
                            <div class="support-content">
                                <h4>Manage your portfolio</h4>
                                <p>Buy and sell popular digital currencies, keep track of them in the one
                                    place.
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- Start About -->
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="support-services ">
                            <a class="support-images" href="#"><i class="flaticon-036-security"></i></a>
                            <div class="support-content">
                                <h4>Recurring buys</h4>
                                <p>Invest in cryptocurrency slowly over time by scheduling buys daily,
                                    weekly,
                                    or monthly.
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- Start services -->
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="support-services ">
                            <a class="support-images" href="#"><i class="flaticon-003-approve"></i></a>
                            <div class="support-content">
                                <h4>Secure Investment</h4>
                                <p>For added security, store your funds in a vault with time delayed
                                    withdrawals.
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- Start services -->
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="support-services">
                            <a class="support-images" href="#"><i class="flaticon-042-wallet"></i></a>
                            <div class="support-content">
                                <h4>Instant withdrawal</h4>
                                <p>
                                    Withdrawals are processed instantly in real time to anywhere in the world
                                </p>
                            </div>
                        </div>
                    </div>

                    <!-- Start services -->
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="support-services">
                            <a class="support-images" href="#"><i class="flaticon-024-megaphone"></i></a>
                            <div class="support-content">
                                <h4>Live customer support</h4>
                                <p>
                                    Top class customer support. readily available to assist our customers at anytime
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Support-service Area -->

    <!-- Start Work proses Area -->
    <div class="work-proses fix bg-color area-padding-2">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="text-center section-headline">
                        <h3>Referral bonus level</h3>
                        <p>Join our Referral Program and earn more dividends.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="text-center work-proses-inner">
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="single-proses">
                                    <div class="proses-content">
                                        <div class="proses-icon point-blue">
                                            <span class="point-view">01</span>
                                            <a href="#"><i class="ti-briefcase"></i></a>
                                        </div>
                                        <div class="proses-text">
                                            <h4>Level 01 instant 10% commission</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End column -->
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="single-proses">
                                    <div class="proses-content">
                                        <div class="proses-icon point-orange">
                                            <span class="point-view">02</span>
                                            <a href="#"><i class="ti-layers"></i></a>
                                        </div>
                                        <div class="proses-text">
                                            <h4>Level 02 instant 20% commission</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End column -->
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="single-proses last-item">
                                    <div class="proses-content">
                                        <div class="proses-icon point-green">
                                            <span class="point-view">03</span>
                                            <a href="#"><i class="ti-bar-chart-alt"></i></a>
                                        </div>
                                        <div class="proses-text">
                                            <h4>Level 03 instant 30% commission</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End column -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Work proses Area -->
    <!--Start payment-history area -->
    {{--<div class="payment-history-area bg-color fix area-padding-2">--}}
    {{-- <div class="container">--}}
    {{-- <div class="row">--}}
    {{-- <div class="col-md-12 col-sm-12 col-xs-12">--}}
    {{-- <div class="text-center section-headline">--}}
    {{-- <h3>Deposit and withdrawals history</h3>--}}
    {{-- <p>Help agencies to define their new business objectives and then create professional software.</p>--}}
    {{-- </div>--}}
    {{-- </div>--}}
    {{-- </div>--}}
    {{-- <div class="row">--}}
    {{-- <div class="col-md-12">--}}
    {{-- <div class="deposite-content">--}}
    {{-- <div class="diposite-box">--}}
    {{-- <h4>Last deposits</h4>--}}
    {{-- <span><i class="flaticon-005-savings"></i></span>--}}
    {{-- <div class="deposite-table">--}}
    {{-- <table>--}}
    {{-- <tr>--}}
    {{-- <th>Name</th>--}}
    {{-- <th>Date</th>--}}
    {{-- <th>Amount</th>--}}
    {{-- <th>Currency</th>--}}
    {{-- </tr>--}}
    {{-- <tr>--}}
    {{-- <td>Admond sayhel</td>--}}
    {{-- <td>Jan 02, 2020</td>--}}
    {{-- <td>$1000</td>--}}
    {{-- <td>Bitcoin</td>--}}
    {{-- </tr>--}}
    {{-- <tr>--}}
    {{-- <td>Jonshon</td>--}}
    {{-- <td>Dec 12, 2019</td>--}}
    {{-- <td>$5000</td>--}}
    {{-- <td>USD</td>--}}
    {{-- </tr>--}}
    {{-- <tr>--}}
    {{-- <td>Hopper</td>--}}
    {{-- <td>Dec 22, 2019</td>--}}
    {{-- <td>$4000</td>--}}
    {{-- <td>Ripple</td>--}}
    {{-- </tr>--}}
    {{-- <tr>--}}
    {{-- <td>Admond sayhel</td>--}}
    {{-- <td>Jan 02, 2020</td>--}}
    {{-- <td>$3000</td>--}}
    {{-- <td>Bitcoin</td>--}}
    {{-- </tr>--}}
    {{-- <tr>--}}
    {{-- <td>Anjel july</td>--}}
    {{-- <td>Jan 05, 2020</td>--}}
    {{-- <td>$500</td>--}}
    {{-- <td>USD</td>--}}
    {{-- </tr>--}}
    {{-- <tr>--}}
    {{-- <td>Lagisha</td>--}}
    {{-- <td>Jan 12, 2020</td>--}}
    {{-- <td>$5000</td>--}}
    {{-- <td>Bitcoin</td>--}}
    {{-- </tr>--}}
    {{-- </table>--}}
    {{-- </div>--}}
    {{-- </div>--}}
    {{-- <div class="diposite-box">--}}
    {{-- <h4>Last withdrawals</h4>--}}
    {{-- <span><i class="flaticon-042-wallet"></i></span>--}}
    {{-- <div class="deposite-table">--}}
    {{-- <table>--}}
    {{-- <tr>--}}
    {{-- <th>Name</th>--}}
    {{-- <th>Date</th>--}}
    {{-- <th>Amount</th>--}}
    {{-- <th>Currency</th>--}}
    {{-- </tr>--}}
    {{-- <tr>--}}
    {{-- <td>Arnold</td>--}}
    {{-- <td>Jan 04, 2020</td>--}}
    {{-- <td>$1000</td>--}}
    {{-- <td>USD</td>--}}
    {{-- </tr>--}}
    {{-- <tr>--}}
    {{-- <td>Jhon Abra</td>--}}
    {{-- <td>Jan 07, 2020</td>--}}
    {{-- <td>$6000</td>--}}
    {{-- <td>USD</td>--}}
    {{-- </tr>--}}
    {{-- <tr>--}}
    {{-- <td>Lanisha</td>--}}
    {{-- <td>Jan 13, 2020</td>--}}
    {{-- <td>$5000</td>--}}
    {{-- <td>USD</td>--}}
    {{-- </tr>--}}
    {{-- <tr>--}}
    {{-- <td>Gongales</td>--}}
    {{-- <td>Jan 12, 2020</td>--}}
    {{-- <td>$2000</td>--}}
    {{-- <td>USD</td>--}}
    {{-- </tr>--}}
    {{-- <tr>--}}
    {{-- <td>Admond sayhel</td>--}}
    {{-- <td>Jan 10, 2020</td>--}}
    {{-- <td>$1000</td>--}}
    {{-- <td>USD</td>--}}
    {{-- </tr>--}}
    {{-- <tr>--}}
    {{-- <td>Remond</td>--}}
    {{-- <td>Jan 02, 2020</td>--}}
    {{-- <td>$3000</td>--}}
    {{-- <td>USD</td>--}}
    {{-- </tr>--}}
    {{-- </table>--}}
    {{-- </div>--}}
    {{-- </div>--}}
    {{-- </div>--}}
    {{-- </div>--}}
    {{-- </div>--}}
    {{-- </div>--}}
    {{--</div>--}}
    <!-- End payment-history area -->
    <!-- Start Banner Area -->
    <div class="banner-area area-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="text-center banner-all area-80">
                        <div class="banner-content">
                            <h3>{{ __("Invest how you want and on what you want") }}</h3>
                            <a class="banner-btn" href="{{ url('register') }}">Sign up now</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- End Banner Area -->
    <!-- Start reviews Area -->
    <div class="reviews-area fix area-padding">
        <div class="container">
            <div class="row">
                <div class="reviews-top">
                    <div class="col-md-5 col-sm-5 col-xs-12">
                        <div class="testimonial-inner">
                            <div class="review-head">
                                <h3>What our customers say</h3>
                                <p>Our customers trust us to deliver great services to meet their needs and this is what
                                    they are saying about our word at Oxenbid</p>
                                <a class="reviews-btn" href=#">More reviews</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-7 col-xs-12">
                        <div class="reviews-content">
                            <!-- start testimonial carousel -->
                            <div class="testimonial-carousel item-indicator">
                                @foreach($testimonials as $testimonial)
                                <div class="single-testi">
                                    <div class="testi-text">
                                        <div class="clients-text">
                                            <div class="client-rating">
                                                <a href="#"><i class="ti-star"></i></a>
                                                <a href="#"><i class="ti-star"></i></a>
                                                <a href="#"><i class="ti-star"></i></a>
                                                <a href="#"><i class="ti-star"></i></a>
                                                <a href="#"><i class="ti-star"></i></a>
                                            </div>
                                            <p>{{ $testimonial->text }}</p>
                                        </div>
                                        <div class="testi-img ">
                                            <img src="{{ asset('uploads/testimonials/'.$testimonial->image) }}" alt="">
                                            <div class="guest-details">
                                                <h4>{{ $testimonial->name }}</h4>
                                                <span class="guest-rev">Client - <a href="#">{{ $testimonial->company }}</a></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End reviews Area -->
    <div class="pt-8 row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="col-xl-12 col-md-12">
                <div style="height:62px; background-color: #FFFFFF; overflow:hidden; box-sizing: border-box; border: 1px solid #56667F; border-radius: 4px; text-align: right; line-height:14px; block-size:62px; font-size: 12px; font-feature-settings: normal; text-size-adjust: 100%; box-shadow: inset 0 -20px 0 0 #56667F;padding:1px;padding: 0px; margin: 0px; width: 100%;">
                    <div style="height:40px; padding:0px; margin:0px; width: 100%;">
                        <iframe src="https://widget.coinlib.io/widget?type=horizontal_v2&theme=light&pref_coin_id=1505&invert_hover=" width="100%" height="36px" scrolling="auto" marginwidth="0" marginheight="0" frameborder="0" border="0" style="border:0;margin:0;padding:0;"></iframe>
                    </div>
                    <div style="color: #FFFFFF; line-height: 14px; font-weight: 400; font-size: 11px; box-sizing: border-box; padding: 2px 6px; width: 100%; font-family: Verdana, Tahoma, Arial, sans-serif;">
                        <a href="https://coinlib.io" target="_blank" style="font-weight: 500; color: #FFFFFF; text-decoration:none; font-size:11px">Cryptocurrency
                            Prices</a>&nbsp;by Coinlib
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Start Footer Area -->
    <footer class="footer1">
        <div class="footer-area bg-inverse">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-sm-5 col-xs-12">
                        <div class="footer-content logo-footer">
                            <div class="footer-head">
                                <div class="footer-logo">
                                    <a class="footer-black-logo" href="#">
                                        <h2 class="text-white" style="color: #ffffff">Oxenbid LTD</h2>
                                    </a>
                                </div>
                                <p> is a commodities broker that provides trading solutions sophisticated enough for veteran
                                    traders, yet simple enough for the forex novice.</p>


                            </div>
                        </div>
                    </div>
                    <!-- end single footer -->
                    <div class="col-md-4 col-sm-3 col-xs-12">
                        <div class="footer-content">
                        </div>
                    </div>
                    <!-- end single footer -->
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="footer-content last-content">
                            <div class="footer-head">
                                <h4>Information</h4>
                                <div class="footer-contacts">
                                    <p><span>Tel :</span> +18324797080</p>
                                    <p><span>Email :</span> info@Oxenbid.com</p>
                                    <p><span>Location :</span> St Clements House, 27-28 Clements Lane, London, EC4N</p>
                                </div>
                                <div class="footer-icons">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-google"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-pinterest"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-instagram"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- End Footer area -->

    <!-- all js here -->

    <!-- jquery latest version -->
    <script src="{{ asset('v2/js/vendor/jquery-1.12.4.min.js') }}"></script>
    <!-- bootstrap js -->
    <script src="{{ asset('v2/js/bootstrap.min.js') }}"></script>
    <!-- owl.carousel js -->
    <script src="{{ asset('v2/js/owl.carousel.min.js') }}"></script>
    <!-- stellar js -->
    <script src="{{ asset('v2/js/jquery.stellar.min.js') }}"></script>
    <!-- magnific js -->
    <script src="{{ asset('v2/js/magnific.min.js') }}"></script>
    <!-- wow js -->
    <script src="{{ asset('v2/js/wow.min.js') }}"></script>
    <!-- meanmenu js -->
    <script src="{{ asset('v2/js/jquery.meanmenu.js') }}"></script>
    <!-- Form validator js -->
    <script src="{{ asset('v2/js/form-validator.min.js') }}"></script>
    <!-- plugins js -->
    <script src="{{ asset('v2/js/plugins.js') }}"></script>
    <!-- main js -->
    <script src="{{ asset('v2/js/main5.js') }}"></script>

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API = Tawk_API || {},
            Tawk_LoadStart = new Date();
        (function() {
            var s1 = document.createElement("script"),
                s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/611ef4e2649e0a0a5cd20239/1fdgep5t3';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>
    <!--End of Tawk.to Script-->
</body>

</html>
