<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Oxenbid') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    @include('partials.assets')
</head>

<body>
    @yield('loader')
    <div id="app">
        @yield('content')
    </div>

    @include('partials.scripts')
    @yield('scripts')

    <script>
        @if(session()->has('success'))
        Toast.fire({
            icon: 'success',
            title: "{{ session()->get('success') }}"
        })
        @elseif(session()->has('error'))
        Toast.fire({
            icon: 'error',
            title: "{{ session()->get('error') }}"
        })
        @elseif(session()->has('warning'))

        Toast.fire({
            icon: 'warning',
            title: "{{ session()->get('warning') }}"
        })
        @endif
    </script>
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API = Tawk_API || {},
            Tawk_LoadStart = new Date();
        (function() {
            var s1 = document.createElement("script"),
                s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/611ef4e2649e0a0a5cd20239/1fdgep5t3';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>
    <!--End of Tawk.to Script-->
</body>

</html>
