<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="navigation">
                    <nav class="navbar navbar-expand-lg navbar-light bg-light">
                        <a class="navbar-brand" href="{{ url('/') }}">
                            Oxenbid
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
                                aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNavDropdown">
                            <ul class="navbar-nav">

                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('/') }}">Home
                                    </a>
{{--                                </li>--}}
{{--                                <li class="nav-item">--}}
{{--                                    <a class="nav-link" href="#">Prices</a>--}}
{{--                                </li>--}}
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('/about') }}">Company
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('contact') }}">Support
                                    </a>
                                </li>
                                @auth
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('account.dashboard', auth()->user()) }}">Dashboard</a>
                                    </li>
                                @endauth

                            </ul>
                        </div>

                        @guest
                            <div class="signin-btn">
                                <a class="btn btn-primary" href="{{ route('login') }}">Sign in</a>
                            </div>
                        @endguest
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
