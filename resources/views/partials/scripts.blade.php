<script src="{{ mix('js/app.js') }}"></script>
<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="{{ asset('vendor/owl-carousel/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/plugins/owl-carousel-init.js') }}"></script>

<script src="{{ asset('vendor/apexchart/apexcharts.min.js') }}"></script>
<script src="{{ asset('vendor/apexchart/apexchart-init.js') }}"></script>
<script src="{{ asset('vendor/toastr/toastr.min.js') }}"></script>

<script src="{{ asset('vendor/waves/waves.min.js') }}"></script>
<script src="{{ asset('vendor/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/plugins/jquery-ui-init.js') }}"></script>
<script src="{{ asset('vendor/validator/jquery.validate.js') }}"></script>
<script src="{{ asset('vendor/validator/validator-init.js') }}"></script>

<script src="{{ asset('js/scripts.js') }}"></script>
