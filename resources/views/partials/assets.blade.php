<!-- Favicon icon -->
<link rel="icon" type="{{ asset('image/png') }}" sizes="16x16" href="images/favicon.png">
<link rel="stylesheet" href="{{ asset('vendor/nice-select/css/nice-select.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/owl-carousel/css/owl.theme.default.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/owl-carousel/css/owl.carousel.min.css') }}">

<link rel="stylesheet" href="{{ asset('vendor/waves/waves.min.css') }}">

<link rel="stylesheet" href="{{ asset('vendor/toastr/toastr.min.css') }}">
<!-- Custom Stylesheet -->
<link rel="stylesheet" href="{{ asset('css/style.css') }}">

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css">

