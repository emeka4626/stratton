@extends('scaffold.base')

@section('body')


<div class="authincation section-padding">
    <div class="container h-100">
        <div class="row justify-content-center h-100 align-items-center">
            <div class="col-xl-5 col-md-6">
                <div class="mini-logo text-center my-5">
{{--                    <img src="images/logo.png" alt="">--}}
                    StrattonTrade
                </div>

                <div class="auth-form card">
                    <div class="card-header justify-content-center">
                        <h4 class="card-title">Reset password</h4>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('password.email') }}">
                            @csrf


                            <div class="form-group">
                                <label for="email">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="hello@example.com">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </form>
{{--                        <div class="new-account mt-3">--}}
{{--                            <p class="mb-1">Don't Received? </p>--}}
{{--                            <a class="text-primary" href="reset.html">Resend </a>--}}
{{--                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
