@extends('scaffold.base')

@section('body')
    <div class="authincation section-padding">
        <div class="container h-100">
            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-xl-5 col-md-6">
                    <div class="my-5 text-center mini-logo">
{{--                        <img src="images/logo.png" alt="">--}}
                        Oxenbid
                    </div>
                    <div class="auth-form card">
                        <div class="card-header justify-content-center">
                            <h4 class="card-title">Sign in</h4>
                        </div>
                        <div class="card-body">
                            <form class="signin_validate" method="POST" action="{{ route('login') }}" autocomplete="off">
                                @csrf
                                <div class="form-group">
                                    <label for="email">{{ __('E-Mail or Username') }}</label>
                                    <input id="email" type="text" class="form-control{{ $errors->has('email') || $errors->has('email') ?  ' is-invalid' : '' }}" placeholder="hello@example.com"
                                           name="email" autocomplete="off" value="{{ old('email') }}" required>

                                    @if($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="password">{{ __('Password') }}</label>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password"
                                           name="password" autocomplete="off">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="mt-4 mb-2 form-row d-flex justify-content-between">
                                    <div class="mb-0 form-group">
                                        <label class="toggle">
                                            <input class="toggle-checkbox" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                            <span class="toggle-switch"></span>
                                            <span class="toggle-label">{{ __('Remember Me') }}</span>
                                        </label>
                                    </div>
                                    @if (Route::has('password.request'))
                                    <div class="mb-0 form-group">
                                        <a href="{{ route('password.request') }}">{{ __('Forgot Your Password?') }}</a>
                                    </div>
                                    @endif
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success btn-block">{{ __('Sign in') }}</button>
                                </div>
                            </form>

                            <div class="mt-3 new-account">
                                <p>Don't have an account? <a class="text-primary" href="{{ route('register') }}">Sign
                                        up</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
