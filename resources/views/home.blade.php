@extends('scaffold.main')

@section('top-section')
    @include('scaffold.includes')
@endsection

@section('body')
    <div class="content-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-3 col-lg-4 col-xxl-4 row">
                    <x-portfolio></x-portfolio>
                    <exchange-component></exchange-component>
                </div>

                <div class="col-xl-6 col-lg-8 col-xxl-8">
                    <div class="card profile_chart">
                        <div class="card-header py-0">
                            <div class="chart_current_data">
                                <h3>254856 <span>USD</span></h3>
                                <p class="text-success">125648 <span>USD (20%)</span></p>
                            </div>
                            <div class="duration-option">
                                <a id="all" class="active">ALL</a>
                                <a id="one_month" class="">1M</a>
                                <a id="six_months">6M</a>
                                <a id="one_year" class="">1Y</a>
                                <a id="ytd" class="">YTD</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="timeline-chart"></div>
                            <div class="chart-content text-center">
                                <div class="pb-4">
                                    <h4 class="card-title">Investment plans</h4>
                                </div>
                                <investment-plans :plans="{{ $plans }}" :user="{{ auth()->user() }}"></investment-plans>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <transactions-component :transactions="{{ $transactions }}"></transactions-component>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header border-0">
                            <h4 class="card-title">Investment History</h4>
                        </div>
                        <div class="card-body pt-0">
                            <div class="transaction-table">
                                <div class="table-responsive">
                                    <table class="table mb-0 table-responsive-sm">
                                        <tr>
                                            <th>id</th>
                                            <th>status</th>
                                            <th>Amount Invested</th>
                                            <th>Plan</th>
                                            <th>Percentage</th>
                                            <th>Earning</th>
                                        </tr>
                                        <tbody>
                                        @foreach($investments as $investment)
                                            <tr>
                                                <td><span class="buy-thumb @if($investment->status == 'PENDING') bg-warning @else bg-success @endif"><i class="la @if($investment->status == 'PENDING') la-arrow-right @else la-arrow-up @endif"></i></span>
                                                </td>

                                                <td>
                                                    <span class="badge @if($investment->status == 'PENDING') badge-warning @else badge-success @endif">{{ $investment->status }}</span>
                                                </td>
                                                <td>+{{ number_format($investment->amount) }} USD</td>
                                                <td>{{ $investment->plan->name }}</td>
                                                <td>{{ $investment->plan->percentage }}</td>
                                                <td>${{ number_format($investment->earning()) }}</td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-scripts')

    <script src="{{ asset('vendor/circle-progress/circle-progress.min.js') }}"></script>
    <script src="{{ asset('vendor/circle-progress/circle-progress-init.js') }}"></script>
    <script src="{{ asset('vendor/toastr/toastr-init.js') }}"></script>
    <script src="{{ asset('js/dashboard.js') }}"></script>
@endsection
