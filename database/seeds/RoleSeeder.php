<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = ['admin'];

        collect($roles)->each(function($name) {
            $role = Role::create([
                'name' => 'admin'
            ]);
        });
    }
}
