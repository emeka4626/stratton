<?php

use App\Plan;
use Illuminate\Database\Seeder;

class PlansSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Plan::truncate();

        $plans = [
            [
                "name" => "Bronze",
                "duration" => 1,
                'percentage' => 3,
                "min" => 50,
                "max" => 900
            ] , [
                "name" => "Silver",
                "duration" => 3,
                'percentage' => 5,
                "min" => 1000,
                "max" => 5000
            ] , [
                "name" => "Gold",
                "duration" => 5,
                'percentage' => 10,
                "min" => 6000,
                "max" => 10000
            ] ,
            [
                "name" => "Platinum",
                "duration" => 7,
                'percentage' => 15,
                "min" => 11000,
                "max" => 15000
            ] ,
            [
                "name" => "Diamond",
                "duration" => 14,
                'percentage' => 20,
                "min" => 16000,
                "max" => 20000
            ] ,[
                "name" => "Vip",
                "duration" => 20,
                'percentage' => 25,
                "min" => 20000,
                "max" => 100000
            ] ,
        ];

        collect($plans)->each(fn($plan) => $this->createPlan($plan));
    }

    private function createPlan(array $plan)
    {
        return Plan::create([
            'name' => $plan['name'],
            'duration' => $plan['duration'],
            'percentage' => $plan['percentage'],
            'min' => $plan['min'],
            'max' => $plan['max']
        ]);
    }
}
