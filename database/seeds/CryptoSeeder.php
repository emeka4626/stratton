<?php

use Illuminate\Database\Seeder;

class CryptoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currencies = array(
            [
                'name' => 'Bitcoin',
                'code' => 'BTC'
            ],
            [
                'name' => 'Etherum',
                'code' => 'ETH',
            ],
            [
                'name' => 'Litecoin',
                'code' => 'LTC',
            ],
            [
                'name' => 'Ripple',
                'code' => 'XRP'
            ],
            [
                'name' => 'Dash',
                'code' => 'DASH'
            ]
        );

        collect($currencies)->each(fn($currency) => \App\Crypto::create([
            'name' => $currency['name'],
            'code' => $currency['code']
        ]));
    }
}
