<?php

use App\Plan;
use App\Testimonial;
use App\User;
use Illuminate\Support\Facades\Auth;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Route;



Route::get('/', function () {
    $testimonials = Testimonial::all();
    $plans = Plan::all();
    $faker = new Faker();
    return view('welcome2', compact('testimonials', 'plans', 'faker'));
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/contact', function () {
    return view('contact');
});


Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');


Route::prefix('profile')->middleware('auth')->group(function () {
    Route::get('create', 'ProfileController@index')->name('profile.create');
    Route::post('create', 'ProfileController@store')->name('profile.store');
});

Route::prefix('account')->middleware(['auth', 'verified', 'has_profile'])->group(function () {
    Route::prefix('{username}')->group(function() {
        Route::get('dashboard', 'HomeController@index')->name('account.dashboard');
        Route::get('buy-sell', 'HomeController@buy')->name('account.buy');
        Route::get('accounts', 'HomeController@accounts')->name('account.accounts');
        Route::get('transactions', 'HomeController@transactions')->name('account.transactions');
        Route::get('settings', 'HomeController@settings')->name('account.settings');
        Route::get('settings-account', 'HomeController@settingsAccount')->name('account.settings-account');
        Route::post('update', 'HomeController@update')->name('account.update');
        Route::post('withdraw', 'WithdrawalController@store')->name('account.withdraw');
        Route::post('wallet/fund', 'AccountController@store')->name('account.fundWallet');

        Route::get('investments', 'InvestmentController@create')->name('investment');
        Route::prefix('transaction')->group(function () {
            Route::get('start', 'InvestmentController@index')->name('invest.index');
            Route::post('invest', 'InvestmentController@store')->name('invest.store');
            Route::get('confirm', 'InvestmentController@show')->name('invest.confirm');
        });
    });

    Route::prefix('verify')->group(function() {
        Route::get('step-1', 'VerificationController@index')->name('verify.step-1');
        Route::get('step-2', 'VerificationController@create')->name('verify.step-2');
        Route::post('step-2', 'VerificationController@store')->name('verify.save');
        Route::middleware('is_verified')->group(function() {
            Route::get('add-bank', 'VerificationController@addBank')->name('verify.add-bank');
            Route::post('add-bank', 'VerificationController@saveBank')->name('verify.save-bank');
            Route::get('add-card', 'VerificationController@addCard')->name('verify.add-card');
            Route::post('save-card', 'VerificationController@saveCard')->name('verify.save-card');
        });
    });

    Route::prefix('referrals')->group(function() {
        Route::get('overview', 'ReferralController@index')->name('referral.index');
    });


});


Route::prefix('admin/{username}')->middleware(['auth', 'admin'])->group(function () {
    Route::get('dashboard', 'Admin\BaseController@index')->name('admin.dashboard');
    Route::get('btc-transactions', 'Admin\BaseController@btcTransactions')->name('admin.btcTransaction');
    Route::get('btc-transactions/pending', 'Admin\BaseController@pendingTransactions')->name('admin.pendingTransactions');
    Route::get('btc-transactions/completed', 'Admin\BaseController@completedTransactions')->name('admin.completedTransactions');
    Route::get('btc-transactions/fund', 'Admin\BaseController@fundWallet')->name('admin.fundWallet');
    Route::post('btc-transactions/fund', 'Admin\BaseController@saveFund')->name('admin.saveFund');
    Route::get('btc-transactions/confirm/{transaction}', 'Admin\BaseController@confirmTransaction')->name('admin.transaction.confirm');

    Route::get('investments', 'Admin\BaseController@investments')->name('admin.investments');
    Route::get('investments/add-fund', 'Admin\BaseController@addFund')->name('admin.investments.addFund');
    Route::post('investments/add-fund', 'Admin\BaseController@fund')->name('admin.investments.fund');
    Route::get('investments/pending', 'Admin\BaseController@pendingInvestments')->name('admin.investments.pending');
    Route::get('investments/confirmed', 'Admin\BaseController@confirmedInvestments')->name('admin.investments.confirmed');
    Route::get('investments/confirm/{investment}', 'Admin\BaseController@confirmInvestment')->name('admin.investment.confirm');

    Route::get('users', 'Admin\BaseController@users')->name('admin.users');
    Route::get('users/delete/{user}', function (User $username, User $user) {
        $user->delete();
        session()->flash('success', 'User deleted successfully');
        return redirect()->back();
    })->name('admin.users.delete');
    Route::get('users/edit/{user}', 'Admin\BaseController@editUser')->name('admin.users.edit');
    Route::post('users/edit/{user}', 'Admin\BaseController@updateUser')->name('admin.users.update');


    Route::get('settings', 'Admin\BaseController@settings')->name('admin.settings');
    Route::get('settings/plans', 'Admin\BaseController@settingsPlans')->name('admin.settings.plans');
    Route::get('settings/plans/create', 'Admin\BaseController@settingsCreatePlan')->name('admin.settings.plans.create');
    Route::post('settings/plans/create', 'Admin\BaseController@createPlan')->name('admin.settings.plans.save');
    Route::get('settings/testimonials', 'Admin\BaseController@settingsTestimonials')->name('admin.settings.testimonials');
    Route::post('settings/testimonials', 'Admin\BaseController@createTestimonial')->name('admin.settings.testimonial.save');

    Route::get('withdrawals', 'Admin\BaseController@withdrawals')->name('admin.withdrawals');

});

Route::prefix('transactions')->middleware('auth')->group(function() {
    Route::post('create', 'TransactionController@store');
});

Route::prefix('blockchain')->group(function () {
    Route::get('callback/{code}', 'BlockChainController@callback')->name('blockchain.callback');
});

Route::post('refer', function () {
    session()->flash('success', 'Referral Link sent successfully');
    return redirect()->back();
})->name('refer');

